﻿using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.Exercise;
using MeFit.Models.DTO.MuscleGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MuscleGroupsController : ControllerBase
    {
        private readonly FitnessDbContext _context;
        private readonly IMapper _mapper;
        readonly string NO_MUSCLEGROUPS = "No musclegroups in database";
        public MuscleGroupsController(FitnessDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all muscle groups from the database
        /// </summary>
        /// <returns>A list of all muscle groups in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MuscleGroupReadDTO>>> GetAllMuscleGroups()
        {
            if (_context.MuscleGroups == null)
            {
                return NotFound(NO_MUSCLEGROUPS);
            }
            List<MuscleGroup> muscleGroups = await _context.MuscleGroups.ToListAsync();
            List<MuscleGroupReadDTO> muscleGroupReadDTOs = _mapper.Map<List<MuscleGroupReadDTO>>(muscleGroups);

            return Ok(muscleGroupReadDTOs);
        }

        /// <summary>
        /// Gets a muscle group based on the given id
        /// </summary>
        /// <param name="id">The id of the muscle group</param>
        /// <returns>The muscle group with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MuscleGroup>> GetMuscleGroupById(int id)
        {
            if (_context.MuscleGroups == null)
            {
                return NotFound(NO_MUSCLEGROUPS);
            }
            var muscleGroup = await _context.MuscleGroups.FindAsync(id);

            if (muscleGroup == null)
            {
                return NotFound($"{NO_MUSCLEGROUPS} with id {id}");
            }

            MuscleGroupReadDTO muscleGroupReadDTO = _mapper.Map<MuscleGroupReadDTO>(muscleGroup);

            return Ok(muscleGroupReadDTO);
        }

        /// <summary>
        /// Gets all the exercises in a muscle group
        /// </summary>
        /// <param name="id">The id of the muscle group that needs to show all the exercises</param>
        /// <returns>A list of exercises that are in the muscle group</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/exercises")]
        public async Task<ActionResult<IEnumerable<ExerciseReadDTO>>> GetAllExercisesFromMuscleGroupById(int id)
        {
            if (_context.MuscleGroups == null)
            {
                return NotFound(NO_MUSCLEGROUPS);
            }
            MuscleGroup muscleGroup = await _context.MuscleGroups
                .Include(m => m.Exercises)
                    .Where(m => m.Id == id).FirstOrDefaultAsync();
            if (muscleGroup == null)
            {
                return NotFound(NO_MUSCLEGROUPS);
            }

            MuscleGroupExerciseReadDTO dto = _mapper.Map<MuscleGroupExerciseReadDTO>(muscleGroup);

            var exercises = await GetExerciseListFromIds(dto.ExerciseIds.ToList());
            List<ExerciseReadDTO> exerciseDTOs = _mapper.Map<List<ExerciseReadDTO>>(exercises);

            return Ok(exerciseDTOs);
        }

        /// <summary>
        /// Updates a muscle group with given ID to the given values. ONLY USERS WITH CONTRIBUTOR ROLE MAY UPDATE
        /// </summary>
        /// <param name="id">The id of the muscle group that needs to be updated</param>
        /// <param name="muscleGroup">All the values required for the updated muscle group</param>
        /// <returns>An action result that represents how the muscle group update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = "Administrator, Contributor")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMuscleGroup(int id, [FromBody] MuscleGroupUpdateDTO muscleGroup)
        {
            if (id != muscleGroup.Id)
            {
                return BadRequest($"Request id {id} doesn't match body id {muscleGroup.Id}");
            }

            if (!MuscleGroupExists(id))
            {
                return NotFound($"{NO_MUSCLEGROUPS} with id {id}");
            }

            var muscleGroupById = await _context.MuscleGroups.FindAsync(id);

            try
            {
                _context.Entry(muscleGroupById).CurrentValues.SetValues(muscleGroup);
                _context.Entry(muscleGroupById).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new muscle group to the database. ONLY USERS WITH CONTRIBUTOR ROLE MAY ADD
        /// </summary>
        /// <param name="muscleGroup">The values for the new muscle group that needs to be added</param>
        /// <returns>An action result that represents how the muscle group adding went</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = "Administrator, Contributor")]
        [HttpPost]
        public async Task<ActionResult<MuscleGroupReadDTO>> AddMuscleGroup([FromBody] MuscleGroupCreateDTO muscleGroup)
        {
            var domainMuscleGroup = _mapper.Map<MuscleGroup>(muscleGroup);

            try
            {
                _context.MuscleGroups.Add(domainMuscleGroup);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("POST request failed because of an internal server error");
            }

            var readMuscleGroupDTO = _mapper.Map<MuscleGroupReadDTO>(domainMuscleGroup);

            return CreatedAtAction("GetMuscleGroupById", new { id = readMuscleGroupDTO.Id }, readMuscleGroupDTO);
        }

        /// <summary>
        /// Removes a muscle group from the database with given ID. ONLY USERS WITH CONTRIBUTOR ROLE MAY DELETE
        /// </summary>
        /// <param name="id">The ID for the muscle group that needs to be deleted</param>
        /// <returns>An action result that represents how the muscle group deletion went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Administrator, Contributor")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMuscleGroup(int id)
        {
            if (_context.MuscleGroups == null)
            {
                return NotFound(NO_MUSCLEGROUPS);
            }
            var muscleGroup = await _context.MuscleGroups.FindAsync(id);
            if (muscleGroup == null)
            {
                return NotFound($"{NO_MUSCLEGROUPS} with id {id}");
            }

            try
            {

                _context.MuscleGroups.Remove(muscleGroup);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("DELETE request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Check whether a muscle group exists
        /// </summary>
        /// <param name="id">The id that need to be checked</param>
        /// <returns>True if the muscle group exists else it returns false</returns>
        [NonAction]
        private bool MuscleGroupExists(int id)
        {
            return (_context.MuscleGroups?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Get all the exercises with the given ids
        /// </summary>
        /// <param name="exerciseIds">A list of exercise ids</param>
        /// <returns>A list of exercises based on the given ids</returns>
        [NonAction]
        private async Task<List<Exercise>> GetExerciseListFromIds(List<int> exerciseIds)
        {
            List<Exercise> selectedExercises = new List<Exercise>();

            for (int i = 0; i < exerciseIds.Count; i++)
            {
                selectedExercises.Add(await _context.Exercises.FindAsync(exerciseIds[i]));
            }

            return selectedExercises;
        }
    }
}
