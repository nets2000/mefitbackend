﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.Exercise
{
    public class ExerciseUpdateDTO
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        [MaxLength(300, ErrorMessage = "Description contains too many characters. Max length is: 300")]
        public string Description { get; set; }

        [MaxLength(250, ErrorMessage = "Image contains too many characters. Max length is: 250")]
        public string Image { get; set; }

        [MaxLength(250, ErrorMessage = "Video link contains too many characters. Max length is: 250")]
        public string VidLink { get; set; }
    }
}
