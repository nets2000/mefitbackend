﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class goalprofilerelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Goals_GoalId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_GoalId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "GoalId",
                table: "Profiles");

            migrationBuilder.AddColumn<bool>(
                name: "Current",
                table: "Goals",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ProfileId",
                table: "Goals",
                type: "int",
                nullable: false,
                defaultValue: 2);

            migrationBuilder.CreateIndex(
                name: "IX_Goals_ProfileId",
                table: "Goals",
                column: "ProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Goals_Profiles_ProfileId",
                table: "Goals",
                column: "ProfileId",
                principalTable: "Profiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Goals_Profiles_ProfileId",
                table: "Goals");

            migrationBuilder.DropIndex(
                name: "IX_Goals_ProfileId",
                table: "Goals");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_GoalId",
                table: "Profiles",
                column: "GoalId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Goals_GoalId",
                table: "Profiles",
                column: "GoalId",
                principalTable: "Goals",
                principalColumn: "Id");
        }
    }
}
