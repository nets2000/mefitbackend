﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.MuscleGroup
{
    public class MuscleGroupCreateDTO
    {

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        public ICollection<int> ExerciseIds { get; set; }
    }
}
