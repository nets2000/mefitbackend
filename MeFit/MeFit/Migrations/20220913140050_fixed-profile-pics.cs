﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class fixedprofilepics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pic",
                table: "ProfilePics",
                newName: "Pic");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Pic",
                table: "ProfilePics",
                newName: "pic");
        }
    }
}
