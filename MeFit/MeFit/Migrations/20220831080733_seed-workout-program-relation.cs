﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class seedworkoutprogramrelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FitnessProgramWorkout_FitnessPrograms_FitnessProgramsId",
                table: "FitnessProgramWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_FitnessProgramWorkout_Workouts_WorkoutsId",
                table: "FitnessProgramWorkout");

            migrationBuilder.RenameColumn(
                name: "WorkoutsId",
                table: "FitnessProgramWorkout",
                newName: "FitnessProgramId");

            migrationBuilder.RenameColumn(
                name: "FitnessProgramsId",
                table: "FitnessProgramWorkout",
                newName: "WorkoutId");

            migrationBuilder.RenameIndex(
                name: "IX_FitnessProgramWorkout_WorkoutsId",
                table: "FitnessProgramWorkout",
                newName: "IX_FitnessProgramWorkout_FitnessProgramId");

            migrationBuilder.InsertData(
                table: "FitnessPrograms",
                columns: new[] { "Id", "Category", "Name" },
                values: new object[] { 1, "", "Workout 1" });

            migrationBuilder.InsertData(
                table: "FitnessPrograms",
                columns: new[] { "Id", "Category", "Name" },
                values: new object[] { 2, "", "Workout 2" });

            migrationBuilder.InsertData(
                table: "FitnessProgramWorkout",
                columns: new[] { "FitnessProgramId", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 2, 4 },
                    { 2, 5 },
                    { 2, 6 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_FitnessProgramWorkout_FitnessPrograms_FitnessProgramId",
                table: "FitnessProgramWorkout",
                column: "FitnessProgramId",
                principalTable: "FitnessPrograms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FitnessProgramWorkout_Workouts_WorkoutId",
                table: "FitnessProgramWorkout",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FitnessProgramWorkout_FitnessPrograms_FitnessProgramId",
                table: "FitnessProgramWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_FitnessProgramWorkout_Workouts_WorkoutId",
                table: "FitnessProgramWorkout");

            migrationBuilder.DeleteData(
                table: "FitnessProgramWorkout",
                keyColumns: new[] { "FitnessProgramId", "WorkoutId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "FitnessProgramWorkout",
                keyColumns: new[] { "FitnessProgramId", "WorkoutId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "FitnessProgramWorkout",
                keyColumns: new[] { "FitnessProgramId", "WorkoutId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "FitnessProgramWorkout",
                keyColumns: new[] { "FitnessProgramId", "WorkoutId" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "FitnessProgramWorkout",
                keyColumns: new[] { "FitnessProgramId", "WorkoutId" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "FitnessProgramWorkout",
                keyColumns: new[] { "FitnessProgramId", "WorkoutId" },
                keyValues: new object[] { 2, 6 });

            migrationBuilder.DeleteData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.RenameColumn(
                name: "FitnessProgramId",
                table: "FitnessProgramWorkout",
                newName: "WorkoutsId");

            migrationBuilder.RenameColumn(
                name: "WorkoutId",
                table: "FitnessProgramWorkout",
                newName: "FitnessProgramsId");

            migrationBuilder.RenameIndex(
                name: "IX_FitnessProgramWorkout_FitnessProgramId",
                table: "FitnessProgramWorkout",
                newName: "IX_FitnessProgramWorkout_WorkoutsId");

            migrationBuilder.AddForeignKey(
                name: "FK_FitnessProgramWorkout_FitnessPrograms_FitnessProgramsId",
                table: "FitnessProgramWorkout",
                column: "FitnessProgramsId",
                principalTable: "FitnessPrograms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FitnessProgramWorkout_Workouts_WorkoutsId",
                table: "FitnessProgramWorkout",
                column: "WorkoutsId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
