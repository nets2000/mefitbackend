﻿namespace MeFit.Models.DTO.Goal
{
    public class GoalAchieveDTO
    {
        public int Id { get; set; }
        public bool Achieved { get; set; }
        public bool Current { get; set; }
        public DateTime EndDate { get; set; }
    }
}
