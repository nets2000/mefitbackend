﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class Profile
    {
        public int Id { get; set; }

        public float Weight { get; set; }

        public float Height { get; set; }

        [MaxLength(1000, ErrorMessage = "Medical conditions contains too many characters. Max length is: 1000")]
        public string MedicalConditions { get; set; }

        [MaxLength(1000, ErrorMessage = "Disabilities contains too many characters. Max length is: 1000")]
        public string Disabilities { get; set; }

        [MaxLength(50, ErrorMessage = "Fitness evaluation contains too many characters. Max length is: 50")]
        public string FitnessEvaluation { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int AddressId { get; set; }

        public Address Address { get; set; }

        public ICollection<FitnessProgram>? FitnessPrograms { get; set; }

        public ICollection<Goal>? Goals { get; set; }
    }
}
