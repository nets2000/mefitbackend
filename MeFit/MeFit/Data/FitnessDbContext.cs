using MeFit.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace MeFit.Data
{
    public class FitnessDbContext : DbContext
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Profile> Profiles { get; set; }

        public DbSet<FitnessProgram> FitnessPrograms { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<ProfilePic> ProfilePics { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<MuscleGroup> MuscleGroups { get; set; }

        public DbSet<GoalWorkout> GoalWorkout { get; set; }
        public DbSet<GoalExercise> GoalExercise { get; set; }

        public FitnessDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data for Exercise
            modelBuilder.Entity<Exercise>().HasData(SeedExercises.GetSeedingExercises());

            // Seeding data MuscleGroup
            modelBuilder.Entity<MuscleGroup>().HasData(SeedMuscleGroups.GetSeedingMuscleGroups());

            // Seed data Workouts
            modelBuilder.Entity<Workout>().HasData(SeedWorkouts.GetSeedingWorkouts());

            // Seed data FitnessPrograms
            modelBuilder.Entity<FitnessProgram>().HasData(SeedFitnessPrograms.GetSeedingFitnessPrograms());

            // many-to-many Exercise-Musclegroup
            modelBuilder.Entity<Exercise>()
                .HasMany(e => e.MuscleGroups)
                .WithMany(m => m.Exercises)
                .UsingEntity<Dictionary<string, object>>(
                    "ExerciseMuscleGroup",
                    r => r.HasOne<MuscleGroup>().WithMany().HasForeignKey("MuscleGroupId"),
                    l => l.HasOne<Exercise>().WithMany().HasForeignKey("ExerciseId"),
                    je =>
                    {
                        je.HasKey("MuscleGroupId", "ExerciseId");
                        je.HasData(
                            new { MuscleGroupId = 4, ExerciseId = 1 },
                            new { MuscleGroupId = 5, ExerciseId = 1 },
                            new { MuscleGroupId = 3, ExerciseId = 2 },
                            new { MuscleGroupId = 4, ExerciseId = 2 },
                            new { MuscleGroupId = 3, ExerciseId = 3 },
                            new { MuscleGroupId = 4, ExerciseId = 3 },
                            new { MuscleGroupId = 6, ExerciseId = 4 },
                            new { MuscleGroupId = 5, ExerciseId = 5 },
                            new { MuscleGroupId = 11, ExerciseId = 6 },
                            new { MuscleGroupId = 1, ExerciseId = 7 },
                            new { MuscleGroupId = 2, ExerciseId = 7 },
                            new { MuscleGroupId = 2, ExerciseId = 8 },
                            new { MuscleGroupId = 1, ExerciseId = 9 },
                            new { MuscleGroupId = 1, ExerciseId = 10 },
                            new { MuscleGroupId = 2, ExerciseId = 11 },
                            new { MuscleGroupId = 9, ExerciseId = 12 },
                            new { MuscleGroupId = 10, ExerciseId = 13 },
                            new { MuscleGroupId = 4, ExerciseId = 14 },
                            new { MuscleGroupId = 5, ExerciseId = 14 },
                            new { MuscleGroupId = 4, ExerciseId = 15 },
                            new { MuscleGroupId = 5, ExerciseId = 15 },
                            new { MuscleGroupId = 4, ExerciseId = 16 },
                            new { MuscleGroupId = 11, ExerciseId = 17 },
                            new { MuscleGroupId = 3, ExerciseId = 18 },
                            new { MuscleGroupId = 4, ExerciseId = 18 },
                            new { MuscleGroupId = 4, ExerciseId = 19 },
                            new { MuscleGroupId = 5, ExerciseId = 19 },
                            new { MuscleGroupId = 7, ExerciseId = 20 },
                            new { MuscleGroupId = 11, ExerciseId = 21 },
                            new { MuscleGroupId = 1, ExerciseId = 22 },
                            new { MuscleGroupId = 2, ExerciseId = 22 },
                            new { MuscleGroupId = 1, ExerciseId = 23 },
                            new { MuscleGroupId = 2, ExerciseId = 24 },
                            new { MuscleGroupId = 9, ExerciseId = 25 },
                            new { MuscleGroupId = 8, ExerciseId = 26 },
                            new { MuscleGroupId = 10, ExerciseId = 27 },
                            new { MuscleGroupId = 11, ExerciseId = 28 },
                            new { MuscleGroupId = 11, ExerciseId = 29 },
                            new { MuscleGroupId = 10, ExerciseId = 30 },
                            new { MuscleGroupId = 1, ExerciseId = 31 },
                            new { MuscleGroupId = 3, ExerciseId = 32 },
                            new { MuscleGroupId = 4, ExerciseId = 32 },
                            new { MuscleGroupId = 11, ExerciseId = 33 },
                            new { MuscleGroupId = 1, ExerciseId = 34 },
                            new { MuscleGroupId = 2, ExerciseId = 34 },
                            new { MuscleGroupId = 9, ExerciseId = 35 },
                            new { MuscleGroupId = 10, ExerciseId = 36 },
                            new { MuscleGroupId = 8, ExerciseId = 37 },
                            new { MuscleGroupId = 2, ExerciseId = 38 },
                            new { MuscleGroupId = 11, ExerciseId = 39 },
                            new { MuscleGroupId = 5, ExerciseId = 40 },
                            new { MuscleGroupId = 3, ExerciseId = 41 },
                            new { MuscleGroupId = 4, ExerciseId = 41 },
                            new { MuscleGroupId = 4, ExerciseId = 42 },
                            new { MuscleGroupId = 1, ExerciseId = 43 },
                            new { MuscleGroupId = 2, ExerciseId = 43 },
                            new { MuscleGroupId = 1, ExerciseId = 44 },
                            new { MuscleGroupId = 1, ExerciseId = 45 },
                            new { MuscleGroupId = 9, ExerciseId = 46 },
                            new { MuscleGroupId = 10, ExerciseId = 47 },
                            new { MuscleGroupId = 2, ExerciseId = 48 },
                            new { MuscleGroupId = 11, ExerciseId = 49 },
                            new { MuscleGroupId = 4, ExerciseId = 50 },
                            new { MuscleGroupId = 5, ExerciseId = 50 },
                            new { MuscleGroupId = 8, ExerciseId = 51 },
                            new { MuscleGroupId = 1, ExerciseId = 52 },
                            new { MuscleGroupId = 10, ExerciseId = 53 },
                            new { MuscleGroupId = 4, ExerciseId = 54 },
                            new { MuscleGroupId = 5, ExerciseId = 54 },
                            new { MuscleGroupId = 8, ExerciseId = 55 },
                            new { MuscleGroupId = 6, ExerciseId = 56 },
                            new { MuscleGroupId = 3, ExerciseId = 57 },
                            new { MuscleGroupId = 4, ExerciseId = 57 },
                            new { MuscleGroupId = 4, ExerciseId = 58 },
                            new { MuscleGroupId = 6, ExerciseId = 58 },
                            new { MuscleGroupId = 4, ExerciseId = 59 },
                            new { MuscleGroupId = 5, ExerciseId = 59 },
                            new { MuscleGroupId = 2, ExerciseId = 60 },
                            new { MuscleGroupId = 3, ExerciseId = 61 },
                            new { MuscleGroupId = 4, ExerciseId = 61 },
                            new { MuscleGroupId = 9, ExerciseId = 62 },
                            new { MuscleGroupId = 12, ExerciseId = 63 },
                            new { MuscleGroupId = 12, ExerciseId = 64 },
                            new { MuscleGroupId = 12, ExerciseId = 65 },
                            new { MuscleGroupId = 12, ExerciseId = 66 },
                            new { MuscleGroupId = 12, ExerciseId = 67 },
                            new { MuscleGroupId = 12, ExerciseId = 68 },
                            new { MuscleGroupId = 12, ExerciseId = 69 },
                            new { MuscleGroupId = 12, ExerciseId = 70 },
                            new { MuscleGroupId = 12, ExerciseId = 71 },
                            new { MuscleGroupId = 12, ExerciseId = 72 },
                            new { MuscleGroupId = 12, ExerciseId = 73 },
                            new { MuscleGroupId = 12, ExerciseId = 74 },
                            new { MuscleGroupId = 11, ExerciseId = 75 },
                            new { MuscleGroupId = 11, ExerciseId = 76 }
                        );
                    });

            // Many-to-many Exercise-Workout
            modelBuilder.Entity<Workout>()
                .HasMany(w => w.Exercises)
                .WithMany(e => e.Workouts)
                .UsingEntity<Dictionary<string, object>>(
                    "ExerciseWorkout",
                    r => r.HasOne<Exercise>().WithMany().HasForeignKey("ExerciseId"),
                    l => l.HasOne<Workout>().WithMany().HasForeignKey("WorkoutId"),
                    je =>
                    {
                        je.HasKey("ExerciseId", "WorkoutId");
                        je.HasData(
                            new { ExerciseId = 1, WorkoutId = 1 },
                            new { ExerciseId = 2, WorkoutId = 1 },
                            new { ExerciseId = 3, WorkoutId = 1 },
                            new { ExerciseId = 4, WorkoutId = 1 },
                            new { ExerciseId = 5, WorkoutId = 1 },
                            new { ExerciseId = 6, WorkoutId = 1 },
                            new { ExerciseId = 7, WorkoutId = 2 },
                            new { ExerciseId = 8, WorkoutId = 2 },
                            new { ExerciseId = 9, WorkoutId = 2 },
                            new { ExerciseId = 10, WorkoutId = 2 },
                            new { ExerciseId = 11, WorkoutId = 2 },
                            new { ExerciseId = 12, WorkoutId = 2 },
                            new { ExerciseId = 13, WorkoutId = 2 },
                            new { ExerciseId = 1, WorkoutId = 3 },
                            new { ExerciseId = 14, WorkoutId = 3 },
                            new { ExerciseId = 15, WorkoutId = 3 },
                            new { ExerciseId = 16, WorkoutId = 3 },
                            new { ExerciseId = 17, WorkoutId = 3 },
                            new { ExerciseId = 1, WorkoutId = 4 },
                            new { ExerciseId = 18, WorkoutId = 4 },
                            new { ExerciseId = 19, WorkoutId = 4 },
                            new { ExerciseId = 16, WorkoutId = 4 },
                            new { ExerciseId = 20, WorkoutId = 4 },
                            new { ExerciseId = 21, WorkoutId = 4 },
                            new { ExerciseId = 22, WorkoutId = 5 },
                            new { ExerciseId = 23, WorkoutId = 5 },
                            new { ExerciseId = 24, WorkoutId = 5 },
                            new { ExerciseId = 25, WorkoutId = 5 },
                            new { ExerciseId = 26, WorkoutId = 5 },
                            new { ExerciseId = 27, WorkoutId = 5 },
                            new { ExerciseId = 28, WorkoutId = 5 },
                            new { ExerciseId = 29, WorkoutId = 5 },
                            new { ExerciseId = 14, WorkoutId = 6 },
                            new { ExerciseId = 15, WorkoutId = 6 },
                            new { ExerciseId = 4, WorkoutId = 6 },
                            new { ExerciseId = 30, WorkoutId = 6 },
                            new { ExerciseId = 31, WorkoutId = 6 },
                            new { ExerciseId = 32, WorkoutId = 6 },
                            new { ExerciseId = 33, WorkoutId = 6 }
                        );
                    });

            // Many-to-many Workout-FitnessProgram
            modelBuilder.Entity<FitnessProgram>()
                .HasMany(f => f.Workouts)
                .WithMany(w => w.FitnessPrograms)
                .UsingEntity<Dictionary<string, object>>(
                    "FitnessProgramWorkout",
                    r => r.HasOne<Workout>().WithMany().HasForeignKey("WorkoutId"),
                    l => l.HasOne<FitnessProgram>().WithMany().HasForeignKey("FitnessProgramId"),
                    je =>
                    {
                        je.HasKey("WorkoutId", "FitnessProgramId");
                        je.HasData(
                            new { WorkoutId = 1, FitnessProgramId = 1 },
                            new { WorkoutId = 2, FitnessProgramId = 1 },
                            new { WorkoutId = 3, FitnessProgramId = 1 },

                            new { WorkoutId = 4, FitnessProgramId = 2 },
                            new { WorkoutId = 5, FitnessProgramId = 2 },
                            new { WorkoutId = 6, FitnessProgramId = 2 }
                        );
                    });
            // Many-to-many Goal-Workout
            modelBuilder.Entity<GoalWorkout>().HasKey(goalWorkout => new { goalWorkout.WorkoutId, goalWorkout.GoalId });
            modelBuilder.Entity<GoalWorkout>()
                .HasOne(goalWorkout => goalWorkout.Goal)
                .WithMany(goalWorkout => goalWorkout.GoalWorkouts)
                .HasForeignKey(goalWorkout => goalWorkout.GoalId);
            modelBuilder.Entity<GoalWorkout>()
                .HasOne(goalWorkout => goalWorkout.Workout)
                .WithMany(w => w.GoalWorkouts)
                .HasForeignKey(goalWorkout => goalWorkout.WorkoutId);
            // Many-to-many Goal-Exercise
            modelBuilder.Entity<GoalExercise>().HasKey(goalWorkout => new { goalWorkout.ExerciseId, goalWorkout.GoalId });
            modelBuilder.Entity<GoalExercise>()
                .HasOne(goalWorkout => goalWorkout.Goal)
                .WithMany(goalWorkout => goalWorkout.GoalExercises)
                .HasForeignKey(goalWorkout => goalWorkout.GoalId);
            modelBuilder.Entity<GoalExercise>()
                .HasOne(goalWorkout => goalWorkout.Exercise)
                .WithMany(w => w.GoalExercises)
                .HasForeignKey(goalWorkout => goalWorkout.ExerciseId);
            #region SETS 
            // Seed data Sets
            // modelBuilder.Entity<Set>().HasData(
            //     new Set { Id = 1, ExerciseId = 1, MinReps = 6, MaxReps = 8 },
            //     new Set { Id = 2, ExerciseId = 2, MinReps = 8, MaxReps = 10 },
            //     new Set { Id = 3, ExerciseId = 3, 6, 10, MinReps = 10, MaxReps = 12 },
            //     new Set { Id = 4, ExerciseId = 4, MinReps = 12, MaxReps = 15 },
            //     new Set { Id = 5, ExerciseId = 5, MinReps = 15, MaxReps = 20 },
            //     new Set { Id = 6, ExerciseId = 6, MinReps = 10, MaxReps = 12 },
            //     
            //     new Set { Id = 7, ExerciseId = 7, MinReps = 6, MaxReps = 8 },
            //     new Set { Id = 8, ExerciseId = 8, MinReps = 10, MaxReps = 12 },
            //     new Set { Id = 9, ExerciseId = 9, MinReps = 8, MaxReps = 10 },
            //     new Set { Id = 10, ExerciseId = 10, MinReps = 10, MaxReps = 12 },
            //     new Set { Id = 11, ExerciseId = 11, MinReps = 12, MaxReps = 15 },
            //     new Set { Id = 12, ExerciseId = 12, MinReps = 15, MaxReps = 20 },
            //     new Set { Id = 13, ExerciseId = 13, MinReps = 10, MaxReps = 12 },
            //     
            //     new Set { Id = 14, ExerciseId = 14, MinReps = 10, MaxReps = 12 },
            //     new Set { Id = 15, ExerciseId = 15, MinReps = 12, MaxReps = 15 },
            //     new Set { Id = 16, ExerciseId = 16, MinReps = 15, MaxReps = 20 },
            //     new Set { Id = 17, ExerciseId = 17, MinReps = 10, MaxReps = 12 }
            // );

            // Many-to-many Sets-Workout
            // modelBuilder.Entity<Workout>()
            //     .HasMany(w => w.Sets)
            //     .WithMany(s => s.Workouts)
            //     .UsingEntity<Dictionary<string, object>>(
            //         "SetWorkout",
            //         r => r.HasOne<Set>().WithMany().HasForeignKey("SetsId"),
            //         l => l.HasOne<Workout>().WithMany().HasForeignKey("WorkoutsId"),
            //         je =>
            //         {
            //             je.HasKey("SetsId", "WorkoutsId");
            //             je.HasData(
            //                 new { SetsId = 1, WorkoutsId = 1 },
            //                 new { SetsId = 2, WorkoutsId = 1 },
            //                 new { SetsId = 3, WorkoutsId = 1 },
            //                 new { SetsId = 4, WorkoutsId = 1 },
            //                 new { SetsId = 5, WorkoutsId = 1 },
            //                 new { SetsId = 6, WorkoutsId = 1 }
            //             );
            //         });
            #endregion
        }
    }
}