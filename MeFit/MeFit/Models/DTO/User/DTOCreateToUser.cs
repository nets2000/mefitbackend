﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.User
{
    public class DTOCreateToUser
    {
        [MaxLength(1000, ErrorMessage = "Password contains too many characters. Max length is: 1000")]
        public string Password { get; set; }

        [MaxLength(100, ErrorMessage = "First name contains too many characters. Max length is: 100")]
        public string FirstName { get; set; }

        [MaxLength(100, ErrorMessage = "Last name contains too many characters. Max length is: 100")]
        public string LastName { get; set; }

        [MaxLength(300, ErrorMessage = "User ID contains too many characters. Max length is: 300")]
        public string UserId { get; set; }

        [MaxLength(5, ErrorMessage = "Wants to be contributor contains too many characters. Max length is: 5")]
        public bool WantsToBeContributor { get; set; }

        [MaxLength(5, ErrorMessage = "Is contributor contains too many characters. Max length is: 5")]
        public bool IsContributor { get; set; }

        [MaxLength(5, ErrorMessage = "Is admin contains too many characters. Max length is: 5")]
        public bool IsAdmin { get; set; }

        public int? ProfilePicId { get; set; }

        public DTOCreateToUser(string password, string firstName, string lastName, string isContributor, string isAdmin, string userId,
            string wantsToBeContributor)
        {
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            IsContributor = bool.Parse(isContributor);
            IsAdmin = bool.Parse(isAdmin);
            UserId = userId;
            WantsToBeContributor = bool.Parse(wantsToBeContributor);
        }
    }
}
