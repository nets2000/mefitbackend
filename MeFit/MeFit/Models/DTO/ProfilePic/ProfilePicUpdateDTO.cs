﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.ProfilePic
{
    public class ProfilePicUpdateDTO
    {
        [MaxLength(500, ErrorMessage = "Name contains too many characters. Max length is: 500")]
        public string Name { get; set; }
        [MaxLength(500, ErrorMessage = "Type contains too many characters. Max length is: 500")]
        public string Type { get; set; }

        public string Pic { get; set; }
    }
}
