﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.Exercise
{
    public class ExerciseCreateDTO
    {
        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        [MaxLength(300, ErrorMessage = "Description contains too many characters. Max length is: 300")]
        public string Description { get; set; }

        public int? TotalSets { get; set; }

        public int? Reps { get; set; }

        public int? MinReps { get; set; }

        public int? MaxReps { get; set; }

        [MaxLength(250, ErrorMessage = "Image contains too many characters. Max length is: 250")]
        public string Image { get; set; }

        [MaxLength(250, ErrorMessage = "Video link contains too many characters. Max length is: 250")]
        public string VidLink { get; set; }
        
        public List<int> MusclegroupIds { get; set; }
        public int UserId { get; set; }
    }
}
