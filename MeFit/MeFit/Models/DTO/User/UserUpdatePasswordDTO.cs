﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.User
{
    public class UserUpdatePasswordDTO
    {
        public int Id { get; set; }

        [MaxLength(1000, ErrorMessage = "Password contains too many characters. Max length is: 1000")]
        public string Password { get; set; }
    }
}
