﻿using MeFit.Models.Domain;
using MeFit.Models.DTO.Address;
using MeFit.Models.DTO.Adress;
using MeFit.Models.DTO.ProfilePic;

namespace MeFit.Profiles
{
    public class ProfilePicProfile : AutoMapper.Profile
    {
        public ProfilePicProfile()
        {
            CreateMap<ProfilePic, ProfilePicReadDTO>();
            CreateMap<CreatePicToProfilePicDTO, ProfilePic>();
        }
    }
}
