﻿using MeFit.Models.Domain;
using MeFit.Models.DTO.Address;
using MeFit.Models.DTO.Adress;

namespace MeFit.Profiles
{
    public class AddressProfile : AutoMapper.Profile
    {
        public AddressProfile()
        {
            CreateMap<Address, AddressReadDTO>();
            CreateMap<Address, AddressReadProfilesDTO>()
                .ForMember(readDTO => readDTO.ProfileIds,
                opt => opt.MapFrom(address => address.Profiles
                .Select(profile => profile.Id)
                .ToList()));
            CreateMap<AddressUpdateDTO, Address>();
            CreateMap<AddressCreateDTO, Address>();
        }
    }
}
