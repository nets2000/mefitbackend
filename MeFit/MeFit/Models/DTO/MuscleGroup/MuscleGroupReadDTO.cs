﻿namespace MeFit.Models.DTO.MuscleGroup
{
    public class MuscleGroupReadDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
