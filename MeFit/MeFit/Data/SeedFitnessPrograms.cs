using MeFit.Models.Domain;

namespace MeFit.Data;

public class SeedFitnessPrograms
{
    public static List<FitnessProgram> GetSeedingFitnessPrograms()
    {
        List<FitnessProgram> fitnessProgramsSeed = new List<FitnessProgram>()
        {
            new FitnessProgram { Id = 1, Name = "Program 1", Category = "" },
            new FitnessProgram { Id = 2, Name = "Program 2", Category = "" }
        };
        return fitnessProgramsSeed;
    }
}