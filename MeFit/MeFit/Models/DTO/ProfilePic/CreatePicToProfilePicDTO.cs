﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.ProfilePic
{
    public class CreatePicToProfilePicDTO
    {
        [MaxLength(500, ErrorMessage = "Name contains too many characters. Max length is: 500")]
        public string Name { get; set; }
        [MaxLength(500, ErrorMessage = "Type contains too many characters. Max length is: 500")]
        public string Type { get; set; }

        public byte[]? Pic { get; set; }

        public CreatePicToProfilePicDTO(string name, string type, string pic)
        {
            Name = name;
            Type = type;
            Pic = Convert.FromBase64String(pic);
        }
    }
}
