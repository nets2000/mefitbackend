using MeFit.Models.Domain;
using MeFit.Models.DTO.FitnessProgram;
using Profile = AutoMapper.Profile;

namespace MeFit.Profiles;

public class FitnessProgramProfile : Profile
{
    public FitnessProgramProfile()
    {
        CreateMap<FitnessProgram, FitnessProgramReadDTO>()
            .ForMember(fprdto => fprdto.WorkoutIds,
                opt => opt
                    .MapFrom(f => f.Workouts
                        .Select(w => w.Id)
                        .ToList()))
            .ForMember(frdto => frdto.ProfileIds,
                opt => opt
                    .MapFrom(e => e.Profiles
                        .Select(m => m.Id)
                        .ToList()));
        CreateMap<FitnessProgramCreateDTO, FitnessProgram>();
        CreateMap<FitnessProgramUpdateDTO, FitnessProgram>();
    }

}