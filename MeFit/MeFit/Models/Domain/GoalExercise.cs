﻿using System.ComponentModel;

namespace MeFit.Models.Domain
{
    public class GoalExercise
    {
        public int GoalId { get; set; }
        public Goal Goal { get; set; }
        public int ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
        [DefaultValue(false)]
        public bool Complete { get; set; }
    }
}
