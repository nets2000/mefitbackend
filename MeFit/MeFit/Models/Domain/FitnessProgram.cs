﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class FitnessProgram
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Category contains too many characters. Max length is: 100")]
        public string Category { get; set; }

        // Do we want to see/know all the profile ids it's added to?
        public ICollection<Profile> Profiles { get; set; }

        public ICollection<Workout> Workouts { get; set; }
    }
}
