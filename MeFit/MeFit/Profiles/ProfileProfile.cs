﻿using MeFit.Models.DTO.Profile;

namespace MeFit.Profiles
{
    public class ProfileProfile : AutoMapper.Profile
    {
        public ProfileProfile()
        {
            CreateMap<Models.Domain.Profile, ProfileReadDTO>();
            CreateMap<Models.Domain.Profile, ProfileReadProgramsDTO>()
                .ForMember(readDTO => readDTO.FitnessProgramIds,
                opt => opt
                    .MapFrom(profile => profile.FitnessPrograms
                        .Select(program => program.Id)
                        .ToList<int>()));
            CreateMap<ProfileCreateDTO, Models.Domain.Profile>();
            CreateMap<ProfileUpdateDTO, Models.Domain.Profile>();
        }
    }
}
