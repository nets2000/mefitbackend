﻿namespace MeFit.Models.DTO.ProfilePic
{
    public class ProfilePicReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public byte[]? Pic { get; set; }
    }
}
