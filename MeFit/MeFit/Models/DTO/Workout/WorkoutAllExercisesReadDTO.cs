﻿namespace MeFit.Models.DTO.Workout
{
    public class WorkoutAllExercisesReadDTO
    {
        public int Id { get; set; }

        public ICollection<int> ExerciseIds { get; set; }
    }
}
