using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.FitnessProgram;
using MeFit.Models.DTO.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Net.Mime;

namespace MeFit.Controllers;

[Route("api/[controller]")]
[ApiController]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
[Authorize]
public class FitnessProgramController : ControllerBase
{
    private readonly FitnessDbContext _context;
    private readonly IMapper _mapper;
    readonly string NO_PROGRAMS = "No fitness programs in database";

    public FitnessProgramController(FitnessDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Get all fitness programs
    /// </summary>
    /// <returns>A list of fitness programs(FitnessProgramReadDTO) from the database</returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<FitnessProgramReadDTO>>> GetAllFitnessPrograms()
    {
        if (_context.FitnessPrograms == null)
        {
            return NotFound(NO_PROGRAMS);
        }
        var fitnessPrograms = await _context.FitnessPrograms
            .Include(f => f.Workouts)
            .Include(f => f.Profiles)
            .ToListAsync();
        var readFitnessPrograms = _mapper.Map<List<FitnessProgramReadDTO>>(fitnessPrograms);

        return readFitnessPrograms;
    }

    /// <summary>
    /// Get a specific fitness program by id
    /// </summary>
    /// <param name="id">The id of the requested fitness program</param>
    /// <returns>One fitness program(FitnessProgramReadDTO) from the database</returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet("{id}")]
    public async Task<ActionResult<FitnessProgramReadDTO>> GetFitnessProgramById(int id)
    {
        if (_context.FitnessPrograms == null)
        {
            return NotFound(NO_PROGRAMS);
        }
        var fitnessProgram = await _context.FitnessPrograms
            .Include(f => f.Workouts)
            .Include(f => f.Profiles)
            .Where(f => f.Id == id)
            .FirstOrDefaultAsync();

        if (!FitnessProgramExists(id))
            return NotFound($"{NO_PROGRAMS} with id {id}");

        var readFitnessProgram = _mapper.Map<FitnessProgramReadDTO>(fitnessProgram);

        return Ok(readFitnessProgram);
    }

    /// <summary>
    /// Gets all the workouts in a program
    /// </summary>
    /// <param name="id">The id of the program that needs to show all the workouts</param>
    /// <returns>A list of workouts that are in the program</returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet("{id}/workouts")]
    public async Task<ActionResult<IEnumerable<WorkoutReadDTO>>> GetAllWorkOutsFromProgramById(int id)
    {
        if (_context.FitnessPrograms == null || _context.Workouts == null)
        {
            return NotFound(NO_PROGRAMS);
        }
        FitnessProgram program = await _context.FitnessPrograms
            .Include(p => p.Workouts)
                .Where(p => p.Id == id).FirstOrDefaultAsync();
        if (program == null)
        {
            return NotFound($"{NO_PROGRAMS} with id {id}");
        }

        FitnessProgramReadDTO dto = _mapper.Map<FitnessProgramReadDTO>(program);

        var workouts = await GetWorkoutListFromIds(dto.WorkoutIds.ToList());
        List<WorkoutReadDTO> workoutDTOs = _mapper.Map<List<WorkoutReadDTO>>(workouts);

        return Ok(workoutDTOs);
    }

    /// <summary>
    /// Adds a fitness program. ONLY USERS WITH CONTRIBUTOR ROLE MAY ADD
    /// </summary>
    /// <param name="fitnessProgramDto">All the fields(FitnessProgramCreateDTO) that need to be added</param>
    /// <returns>The fitness program(FitnessProgramReadDTO) that is successfully added to the database</returns>
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Authorize(Roles = "Administrator, Contributor")]
    [HttpPost]
    public async Task<ActionResult<FitnessProgramReadDTO>> AddFitnessProgram([FromBody] FitnessProgramCreateDTO fitnessProgramDto)
    {
        var domainFitnessProgram = _mapper.Map<FitnessProgram>(fitnessProgramDto);
        try
        {
            _context.FitnessPrograms.Add(domainFitnessProgram);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("POST request failed because of an internal server error");
        }

        var newFitnessProgram = _mapper.Map<FitnessProgramReadDTO>(domainFitnessProgram);

        return CreatedAtAction("GetFitnessProgramById", new { Id = newFitnessProgram.Id }, newFitnessProgram);
    }

    /// <summary>
    /// Updates an fitness program found by id. ONLY USERS WITH CONTRIBUTOR ROLE MAY UPDATE
    /// </summary>
    /// <param name="id">The id of the fitness program that needs to be updated</param>
    /// <param name="updatedFitnessProgram">All the fields(FitnessProgramUpdateDTO) that need to be updated</param>
    /// <returns>The updated fitness program when successfully updated</returns>
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Authorize(Roles = "Administrator, Contributor")]
    [HttpPut]
    public async Task<ActionResult> UpdateFitnessProgram(int id, [FromBody] FitnessProgramUpdateDTO updatedFitnessProgram)
    {
        if (id != updatedFitnessProgram.Id)
            return BadRequest($"Request id {id} doesn't match body id {updatedFitnessProgram.Id}");

        if (!FitnessProgramExists(id))
            return NotFound($"{NO_PROGRAMS} with id {id}");

        try
        {
            var domainFitnessProgram = _mapper.Map<FitnessProgram>(updatedFitnessProgram);
            _context.Entry(domainFitnessProgram).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("PUT request failed because of an internal server error");
        }

        return Ok(updatedFitnessProgram);
    }

    /// <summary>
    /// Update the workouts of the program. ONLY USERS WITH CONTRIBUTOR ROLE MAY UPDATE
    /// </summary>
    /// <param name="id">The id of the program that needs to be updated</param>
    /// <param name="workoutIds">The ids of workouts that the program needs to have</param>
    /// <returns>An action result of how the update went</returns>
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [Authorize(Roles = "Administrator, Contributor")]
    [HttpPut("{id}/workouts")]
    public async Task<ActionResult> UpdateWorkoutsFromProgram(int id, List<int> workoutIds)
    {
        FitnessProgram selectedProgram = await _context.FitnessPrograms
            .Include(p => p.Workouts)
                .Where(p => p.Id == id).FirstOrDefaultAsync();

        if (selectedProgram == null)
        {
            return NotFound($"{NO_PROGRAMS} with id {id}");
        }

        try
        {
            List<Workout> selectedWorkouts = await GetWorkoutListFromIds(workoutIds);

            selectedProgram.Workouts = selectedWorkouts;
            _context.Entry(selectedProgram).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("PUT request failed because of an internal server error");
        }

        return NoContent();
    }

    /// <summary>
    /// Deletes an exercise by id. ONLY USERS WITH CONTRIBUTOR ROLE MAY DELETE
    /// </summary>
    /// <param name="id">The id of the exercise that needs to be deleted</param>
    /// <returns>Saves changes to the database, but doesn't return anything</returns>
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Authorize(Roles = "Administrator, Contributor")]
    [HttpDelete]
    public async Task<ActionResult> DeleteFitnessProgram(int id)
    {
        var fitnessProgram = await _context.FitnessPrograms.FindAsync(id);
        if (!FitnessProgramExists(id))
            return NotFound($"{NO_PROGRAMS} with id {id}");

        try
        {
            var domainFitnessProgram = _mapper.Map<FitnessProgram>(fitnessProgram);
            _context.FitnessPrograms.Remove(domainFitnessProgram);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("DELETE request failed because of an internal server error");
        }

        return NoContent();
    }

    /// <summary>
    /// Helper method to check whether the fitness program exists in the database
    /// </summary>
    /// <param name="id">The id of the fitness program to be checked</param>
    /// <returns>True if exercise exists, but false if it doesn't</returns>
    [NonAction]
    private bool FitnessProgramExists(int id)
    {
        return _context.FitnessPrograms.Any(f => f.Id == id);
    }

    /// <summary>
    /// Get all the workouts with the given ids
    /// </summary>
    /// <param name="workoutIds">A list of workout ids</param>
    /// <returns>A list of workouts based on the given ids</returns>
    [NonAction]
    private async Task<List<Workout>> GetWorkoutListFromIds(List<int> workoutIds)
    {
        List<Workout> selectedWorkouts = new List<Workout>();

        for (int i = 0; i < workoutIds.Count; i++)
        {
            selectedWorkouts.Add(await _context.Workouts.FindAsync(workoutIds[i]));
        }

        return selectedWorkouts;
    }
}