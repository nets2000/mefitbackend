﻿using MeFit.Models.Domain;
using MeFit.Models.DTO.User;

namespace MeFit.Profiles
{
    public class UserProfile : AutoMapper.Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>();
            CreateMap<DTOCreateToUser, User>();
            CreateMap<DTOUpdateToUser, User>();
            CreateMap<UserUpdatePasswordDTO, User>();
        }
    }
}
