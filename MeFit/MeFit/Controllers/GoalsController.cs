﻿using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.Exercise;
using MeFit.Models.DTO.Goal;
using MeFit.Models.DTO.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;

namespace MeFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GoalsController : ControllerBase
    {
        private readonly string NO_GOALS = "No goals in database";
        private readonly FitnessDbContext _context;
        private readonly IMapper _mapper;

        public GoalsController(FitnessDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all goals from the database
        /// </summary>
        /// <returns>A list of all goals in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GoalReadDTO>>> GetAllGoals()
        {
            if (_context.Goals == null)
            {
                return NotFound(NO_GOALS);
            }

            return _mapper.Map<List<GoalReadDTO>>(await _context.Goals.ToListAsync());
        }

        /// <summary>
        /// Get a goal with a specific id from the database
        /// </summary>
        /// <param name="id">GoalId to retrieve from database</param>
        /// <returns>A goal with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<GoalReadDTO>> GetGoalById(int id)
        {
            if (_context.Goals == null)
            {
                return NotFound(NO_GOALS);
            }
            var goals = _mapper.Map<GoalReadDTO>(await _context.Goals.Where(g => g.Id == id).FirstOrDefaultAsync());
            return Ok(goals);
        }

        /// <summary>
        /// Gets all the workouts in a goal
        /// </summary>
        /// <param name="id">The id of the goal that needs to show all the workouts</param>
        /// <returns>A list of workouts that are in the goal</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/workouts")]
        public async Task<ActionResult<List<WorkoutGoalReadDTO>>> GetAllWorkOutsFromGoalById(int id)
        {
            if (_context.Goals == null)
            {
                return NotFound(NO_GOALS);
            }
            if (!GoalExists(id))
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            var workouts = await _context.GoalWorkout
                .Where(g => g.GoalId == id).Select(gw => gw.WorkoutId).ToListAsync();
            if (workouts == null)
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            List<WorkoutGoalReadDTO> goalworkouts = new();
            foreach (var wid in workouts)
            {
                var foundWorkout = await _context.Workouts.FindAsync(wid);
                if (foundWorkout != null)
                {
                    goalworkouts.Add(new WorkoutGoalReadDTO
                    {
                        Id = foundWorkout.Id,
                        Name = foundWorkout.Name,
                        Type = foundWorkout.Type,
                        Complete = await _context.GoalWorkout.Where(gw => gw.WorkoutId == foundWorkout.Id && gw.GoalId == id).Select(gw => gw.Complete).FirstAsync()
                    });
                }

            }
            return Ok(goalworkouts);
        }

        /// <summary>
        /// Gets all the exercises in a goal
        /// </summary>
        /// <param name="id">The id of the goal that needs to show all the exercises</param>
        /// <returns>A list of exercises that are in the goal</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/exercises")]
        public async Task<ActionResult<List<ExerciseGoalReadDTO>>> GetAllExercisesFromGoalById(int id)
        {
            if (_context.Goals == null)
            {
                return NotFound(NO_GOALS);
            }
            if (!GoalExists(id))
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            var exercises = await _context.GoalExercise
                .Where(g => g.GoalId == id).Select(gw => gw.ExerciseId).ToListAsync();
            if (exercises == null)
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            List<ExerciseGoalReadDTO> goalExercises = new();
            foreach (var wid in exercises)
            {
                var foundExercise = await _context.Exercises.FindAsync(wid);
                if (foundExercise != null)
                {
                    goalExercises.Add(new ExerciseGoalReadDTO
                    {
                        Id = foundExercise.Id,
                        Name = foundExercise.Name,
                        Complete = await _context.GoalExercise.Where(gw => gw.ExerciseId == foundExercise.Id && gw.GoalId == id).Select(gw => gw.Complete).FirstAsync()
                    });
                }

            }
            return Ok(goalExercises);
        }

        /// <summary>
        /// Update a goal in the database
        /// </summary>
        /// <param name="id">GoalId to update in database</param>
        /// <param name="goal">Updated values to replace with current ones in database</param>
        /// <returns>ActionResult with statuscode 400 if id doesnt match between query and body, 404 if database contains no
        /// goal with given id or 204 if PATCH was succesful</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateGoal(int id, GoalUpdateDTO goal)
        {
            if (id != goal.Id)
            {
                return BadRequest($"Request id {id} doesn't match body id {goal.Id}");
            }
            var oldGoal = await _context.Goals.FindAsync(id);
            try
            {
                if (oldGoal != null)
                {
                    _context.Entry(oldGoal).CurrentValues.SetValues(UpdateValues(oldGoal, goal));
                }
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GoalExists(id))
                {
                    return NotFound($"{NO_GOALS} with id {id}");
                }
                else
                {
                    return Problem("PATCH request failed because of an internal server error");
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Update a goal in the database to be set to achieved
        /// </summary>
        /// <param name="id">GoalId to update in database</param>
        /// <param name="goal">Updated values to replace with current ones in database</param>
        /// <returns>ActionResult with statuscode 400 if id doesnt match between query and body, 404 if database contains no
        /// goal with given id or 204 if PATCH was succesful</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPatch("{id}/achieve")]
        public async Task<IActionResult> AchieveGoal(int id, GoalAchieveDTO goal)
        {
            if (!GoalExists(id))
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            if (id != goal.Id)
            {
                return BadRequest($"Request id {id} doesn't match body id {goal.Id}");
            }
            var oldGoal = await _context.Goals.FindAsync(id);
            if (oldGoal == null)
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            try
            {
                oldGoal.Achieved = goal.Achieved;
                oldGoal.Current = goal.Current;
                oldGoal.EndDate = goal.EndDate;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Problem("PUT request failed because of an internal server error");
            }
            return NoContent();
        }

        /// <summary>
        /// Update the workouts of the goal
        /// </summary>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/workouts")]
        public async Task<ActionResult> UpdateWorkoutsFromGoal(int id, List<int> workoutIds)
        {
            if (!GoalExists(id))
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            Goal selectedGoal = await _context.Goals.FindAsync(id);
            List<GoalWorkout> goalWorkouts = new List<GoalWorkout>();
            foreach (int ids in workoutIds)
            {
                Workout workout = await _context.Workouts.FindAsync(ids);
                if (workout != null)
                {
                    var newGoalWorkout = new GoalWorkout()
                    {
                        Goal = selectedGoal,
                        GoalId = selectedGoal.Id,
                        Workout = workout,
                        WorkoutId = workout.Id,
                        Complete = false
                    };
                    goalWorkouts.Add(newGoalWorkout);
                    if (workout.GoalWorkouts != null)
                        workout.GoalWorkouts.Add(newGoalWorkout);
                    else workout.GoalWorkouts = new List<GoalWorkout> { newGoalWorkout };
                }
            }

            selectedGoal.GoalWorkouts = goalWorkouts;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Problem("PUT request failed because of an internal server error");
            }
            return NoContent();
        }

        /// <summary>
        /// Update the exercises of the goal
        /// </summary>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/exercises")]
        public async Task<ActionResult> UpdateExercisesFromGoal(int id, List<int> workoutIds)
        {
            if (!GoalExists(id))
            {
                return NotFound($"{NO_GOALS} with id {id}");
            }
            Goal selectedGoal = await _context.Goals.FindAsync(id);
            List<GoalExercise> goalExercises = new List<GoalExercise>();
            foreach (int ids in workoutIds)
            {
                Exercise exercise = await _context.Exercises.FindAsync(ids);
                if (exercise != null)
                {
                    var newGoalExercise = new GoalExercise()
                    {
                        Goal = selectedGoal,
                        GoalId = selectedGoal.Id,
                        Exercise = exercise,
                        ExerciseId = exercise.Id,
                        Complete = false
                    };
                    goalExercises.Add(newGoalExercise);
                    if (exercise.GoalExercises != null)
                    {
                        exercise.GoalExercises.Add(newGoalExercise);
                    }
                    else 
                    {
                        exercise.GoalExercises = new List<GoalExercise> { newGoalExercise };
                    }
                }
            }
            selectedGoal.GoalExercises = goalExercises;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Problem("PUT request failed because of an internal server error");
            }
            return NoContent();
        }

        /// <summary>
        /// Create a new goal in the database
        /// </summary>
        /// <param name="goal">Goal to add to the database</param>
        /// <returns>The added goal object if POST was succesful, or a Problem if not</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<GoalReadDTO>> AddGoal(GoalCreateDTO goal)
        {
            if (goal == null || goal == default  || goal.EndDate == default|| goal.ProfileId == default|| goal.ToJson().Length == 0)
            {
                return BadRequest();
            }
            Goal g = new Goal();
            try
            {
                g.EndDate = goal.EndDate;
                g.Current = true;
                g.Achieved = false;
                g.FitnessProgramId = goal.FitnessProgramId;
                g.ProfileId = goal.ProfileId;
                var goalWorkoutList = new List<GoalWorkout>();
                var goalExerciseList = new List<GoalExercise>();
                if (goal.WorkoutIds != null)
                {
                    var workouts = new List<Workout>();
                    foreach (int id in goal.WorkoutIds)
                    {
                        var workout = await _context.Workouts.FindAsync(id);
                        if (workout != null)
                        {
                            var goalWorkout = new GoalWorkout()
                            {
                                Complete = false,
                                Goal = g,
                                Workout = workout,
                                GoalId = g.Id,
                                WorkoutId = workout.Id
                            };
                            goalWorkoutList.Add(goalWorkout);
                            if (workout.GoalWorkouts != null)
                                workout.GoalWorkouts.Add(goalWorkout);
                            else workout.GoalWorkouts = new List<GoalWorkout>() { goalWorkout };
                            _context.Entry(workout).State = EntityState.Modified;
                        }
                    }
                }
                if (goal.ExerciseIds != null)
                {
                    var exercises = new List<Exercise>();
                    foreach (int id in goal.ExerciseIds)
                    {
                        var exercise = await _context.Exercises.FindAsync(id);
                        if (exercise != null)
                        {
                            var goalExercise = new GoalExercise()
                            {
                                Complete = false,
                                Goal = g,
                                Exercise = exercise,
                                GoalId = g.Id,
                                ExerciseId = exercise.Id
                            };
                            goalExerciseList.Add(goalExercise);
                            if (exercise.GoalExercises != null)
                                exercise.GoalExercises.Add(goalExercise);
                            else exercise.GoalExercises = new List<GoalExercise>() { goalExercise };
                            _context.Entry(exercise).State = EntityState.Modified;
                        }
                    }
                }
                g.GoalWorkouts = goalWorkoutList;
                g.GoalExercises = goalExerciseList;
                _context.Goals.Add(g);
                await _context.GoalWorkout.AddRangeAsync(goalWorkoutList);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return Problem($"POST request failed because of an internal server error: {ex.Message}");
            }
            return CreatedAtAction("GetGoalById", new { id = g.Id }, _mapper.Map<GoalReadDTO>(g));
        }

        /// <summary>
        /// Deletes a goal with a given id from the database
        /// </summary>
        /// <param name="id">GoalId to delete from database</param>
        /// <returns>ActionResult with statuscode 404 if database contains no goals or goal with given id, or 204 of DELETE
        /// was succesful</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGoal(int id)
        {
            if (_context.Goals == null)
            {
                return NotFound(NO_GOALS);
            }
            var goal = await _context.Goals.FindAsync(id);
            if (goal == null)
            {
                return NotFound(NO_GOALS + $" with id {id}");
            }

            _context.Goals.Remove(goal);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Check whether a goal exists
        /// </summary>
        /// <param name="id">The id that need to be checked</param>
        /// <returns>True if the goal exists else it returns false</returns>
        [NonAction]
        private bool GoalExists(int id)
        {
            return (_context.Goals?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Updates an original object to the updated object
        /// </summary>
        /// <param name="originalGoal">The original object</param>
        /// <param name="updatedGoal">The updated object</param>
        /// <returns>The original object with the updated values</returns>
        private object UpdateValues(object originalGoal, object updatedGoal)
        {
            foreach (var property in updatedGoal.GetType().GetProperties())
            {
                if (property.GetValue(updatedGoal, null) == null)
                {
                    property.SetValue(updatedGoal, originalGoal.GetType().GetProperty(property.Name).GetValue(originalGoal, null));
                }
            }

            return updatedGoal;
        }
    }
}
