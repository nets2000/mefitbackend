﻿namespace MeFit.Models.DTO.Workout
{
    public class WorkoutReadDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Type { get; set; }
    }
}
