﻿namespace MeFit.Models.DTO.FitnessProgram
{
    public class FitnessProgramReadDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public ICollection<int> ProfileIds { get; set; }

        public ICollection<int> WorkoutIds { get; set; }
    }
}
