# MeFit Backend
This is the back-end for the MeFit application. In this back-end new users can be created for the MeFit program. A new user exists out of a profile, user and address class which are all connected. Each user can have goals. 
Each goal has a program. Each program has workouts, with each workout having exercises. If you're a contributor to MeFit you can create new programs, workouts and exercises. Each exercise also has muscle groups connected to it.
Read the API documentation to see how all end points work. Check the Database design to see how all tables are connected to each other.
 
# Installation & start-up
Before you start installing the project make sure the following is installed on you're computer:
* [.NET 6.0](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)

Then fork to project to you're own git repository or download the zip file.<br>
Then open the project in visual studio. Add a new file to the project in the root folder called: appsettings.json. Add the following lines to the file:<br>
![alt text](/MeFit/MeFit/ReadMeImages/ReadMeImage1.PNG?raw=true)<br>
Instead of 'You're connection string' add the connection string that is given to you.<br>

If you haven't selected a solution yet, double click the following file:<br>
![alt text](/MeFit/MeFit/ReadMeImages/ReadMeImage2.PNG?raw=true)<br>

Make sure all the following packages are installed in the solution:
  * AutoMapper.Extensions.Microsoft.DependencyInjection (11.0.0)
  * Microsoft.AspNet.WebApi.WebHost (5.2.9)
  * Microsoft.AspNetCore.Authentication.JwtBearer (6.0.8)
  * Microsoft.EntityFrameworkCore.SqlServer (6.0.8)
  * Microsoft.EntityFrameworkCore.Tools (6.0.8)
  * Microsoft.VisualStudio.Web.CodeGeneration.Design (6.0.8)
  * Swashbuckle.AspNetCore (6.4.0)

To check whether you have all the packages installed, open the NuGet package manager:<br>
![alt text](/MeFit/MeFit/ReadMeImages/ReadMeImage3.PNG?raw=true)<br>
Then open the installed page:
![alt text](/MeFit/MeFit/ReadMeImages/ReadMeImage4.PNG?raw=true)<br>
If there are no packages installed then open the browse page and search and download all the above given packages with the correct versions:<br>
![alt text](/MeFit/MeFit/ReadMeImages/ReadMeImage5.PNG?raw=true)<br>

After this is done you can click the start button:<br>
![alt text](/MeFit/MeFit/ReadMeImages/ReadMeImage6.PNG?raw=true)

# Usage
At the moment the entire database is secured and Swagger can't get any data. To make Swagger can show data you have to remove the following line from a Controller script (in the folder 'Controller' and then for example: AddressController):<br>
![alt text](/MeFit/MeFit/ReadMeImages/ReadMeImage7.PNG?raw=true)<br>
Run the project, Swagger will open the interface automatically.<br>
Send HTTP Requests using the Swagger interface to try out the API!

# Project Structure
* Controllers: The HTTP Controllers for the different models in the database
* Data: The Database Context needed for EntityFramework and the Seeded Data
* Migrations: Database migrations needed to run update-database to add the database, tables and seeded data
* Models: In the domain folder each object has a base template for what they look like and the DTO (Data Translation Object) folder has all the DTO's for each of the objects.
* Profiles: Configuration for AutoMapper to process from model to DTO and from DTO to model

# Authors
Sten de Boer: https://gitlab.com/nets2000 <br>
Fenna Ransijn: https://gitlab.com/fransijn <br>
Nienke Kapers: https://gitlab.com/nienkek2101

