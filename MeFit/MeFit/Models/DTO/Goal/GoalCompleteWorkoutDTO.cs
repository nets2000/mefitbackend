﻿namespace MeFit.Models.DTO.Goal
{
    public class GoalCompleteWorkoutDTO
    {
        public int GoalId { get; set; }
        public bool Complete { get; set; }
    }
}
