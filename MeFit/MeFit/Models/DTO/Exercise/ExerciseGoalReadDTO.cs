﻿namespace MeFit.Models.DTO.Exercise
{
    public class ExerciseGoalReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Complete { get; set; }
    }
}
