﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class addrepsinexercises : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DropForeignKey(
            //     name: "FK_ExerciseWorkout_Workout_WorkoutId",
            //     table: "ExerciseWorkout");
            //
            // migrationBuilder.DropForeignKey(
            //     name: "FK_Profiles_User_UserId",
            //     table: "Profiles");
            //
            // migrationBuilder.DropTable(
            //     name: "User");
            //
            // migrationBuilder.DropTable(
            //     name: "Workout");

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndDate",
                table: "Goals",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20);

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 8, 6, 4 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 10, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 8, 6, 4 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 10, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 4 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 10, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 10, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 10, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 10, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 8, 6, 4 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 8, 6, 4 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 15, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { 20, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 20, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { 15, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { 12, 10, 3 });

            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "Image", "MaxReps", "MinReps", "Name", "Reps", "TotalSets", "VidLink" },
                values: new object[,]
                {
                    { 75, "", "", 25, 20, "Half sit up", null, 3, "" },
                    { 76, "", "", 25, 20, "Reverse crunch & leg raise", null, 3, "" }
                });

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Program 1");

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Program 2");

            migrationBuilder.InsertData(
                table: "ExerciseMuscleGroup",
                columns: new[] { "ExerciseId", "MuscleGroupId" },
                values: new object[] { 75, 11 });

            migrationBuilder.InsertData(
                table: "ExerciseMuscleGroup",
                columns: new[] { "ExerciseId", "MuscleGroupId" },
                values: new object[] { 76, 11 });

            // migrationBuilder.AddForeignKey(
            //     name: "FK_ExerciseWorkout_Workouts_WorkoutId",
            //     table: "ExerciseWorkout",
            //     column: "WorkoutId",
            //     principalTable: "Workouts",
            //     principalColumn: "Id",
            //     onDelete: ReferentialAction.Cascade);
            //
            // migrationBuilder.AddForeignKey(
            //     name: "FK_Profiles_Users_UserId",
            //     table: "Profiles",
            //     column: "UserId",
            //     principalTable: "Users",
            //     principalColumn: "Id",
            //     onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseWorkout_Workouts_WorkoutId",
                table: "ExerciseWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Users_UserId",
                table: "Profiles");

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExerciseId", "MuscleGroupId" },
                keyValues: new object[] { 75, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExerciseId", "MuscleGroupId" },
                keyValues: new object[] { 76, 11 });

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.AlterColumn<string>(
                name: "EndDate",
                table: "Goals",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    TempId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.UniqueConstraint("AK_User_TempId", x => x.TempId);
                });

            migrationBuilder.CreateTable(
                name: "Workout",
                columns: table => new
                {
                    TempId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.UniqueConstraint("AK_Workout_TempId", x => x.TempId);
                });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Reps", "TotalSets" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "MaxReps", "MinReps", "TotalSets" },
                values: new object[] { null, null, null });

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Workout 1");

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Workout 2");

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseWorkout_Workout_WorkoutId",
                table: "ExerciseWorkout",
                column: "WorkoutId",
                principalTable: "Workout",
                principalColumn: "TempId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_User_UserId",
                table: "Profiles",
                column: "UserId",
                principalTable: "User",
                principalColumn: "TempId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
