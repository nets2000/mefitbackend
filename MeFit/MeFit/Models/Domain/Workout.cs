﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class Workout
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Type contains too many characters. Max length is: 100")]
        public string Type { get; set; }
        public int UserId { get; set; }

        public User User { get; set; }

        public ICollection<GoalWorkout> GoalWorkouts { get; set; }

        public ICollection<FitnessProgram> FitnessPrograms { get; set; }

        public ICollection<Exercise> Exercises { get; set; }
    }
}
