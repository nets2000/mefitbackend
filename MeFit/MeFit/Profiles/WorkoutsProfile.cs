﻿using MeFit.Models.Domain;
using MeFit.Models.DTO.Workout;
namespace MeFit.Profiles
{
    public class WorkoutsProfile : AutoMapper.Profile
    {
        public WorkoutsProfile()
        {
            CreateMap<Workout, WorkoutAllExercisesReadDTO>()
                .ForMember(readDTO => readDTO.ExerciseIds,
                opt => opt.MapFrom(workout => workout.Exercises
                    .Select(exercise => exercise.Id)
                    .ToList<int>()));
            CreateMap<Workout, WorkoutCreateDTO>()
                .ForMember(w => w.UserId, 
                opt =>{
                    opt
                    .MapFrom(w => w.User.UserId);
                }).ReverseMap();
            CreateMap<Workout, WorkoutReadDTO>();
            CreateMap<WorkoutUpdateDTO, WorkoutReadDTO>();
            CreateMap<WorkoutUpdateDTO, Workout>();
        }
    }
}
