﻿namespace MeFit.Models.DTO.Profile
{
    public class ProfileReadProgramsDTO
    {
        public int Id { get; set; }

        public ICollection<int>? FitnessProgramIds { get; set; }
    }
}
