﻿using MeFit.Models.Domain;
using MeFit.Models.DTO.Goal;
namespace MeFit.Profiles
{
    public class GoalsProfile : AutoMapper.Profile
    {
        public GoalsProfile()
        {
            CreateMap<Goal, GoalReadDTO>();
            CreateMap<GoalCreateDTO, Goal>();
            CreateMap<GoalUpdateDTO, GoalReadDTO>();
            CreateMap<GoalUpdateDTO, Goal>();
        }
    }
}
