﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.User
{
    public class DTOUpdateToUser
    {
        [MaxLength(100, ErrorMessage = "First name contains too many characters. Max length is: 100")]
        public string FirstName { get; set; }

        [MaxLength(100, ErrorMessage = "Last name contains too many characters. Max length is: 100")]
        public string LastName { get; set; }

        [MaxLength(5, ErrorMessage = "Wants to be contributor contains too many characters. Max length is: 5")]
        public bool WantsToBeContributor { get; set; }

        [MaxLength(5, ErrorMessage = "Is contributor contains too many characters. Max length is: 5")]
        public bool IsContributor { get; set; }

        [MaxLength(5, ErrorMessage = "Is admin contains too many characters. Max length is: 5")]
        public bool IsAdmin { get; set; }

        public DTOUpdateToUser(string firstName, string lastName, string isContributor, string isAdmin, string wantsToBeContributor)
        {
            FirstName = firstName;
            LastName = lastName;
            IsContributor = bool.Parse(isContributor);
            IsAdmin = bool.Parse(isAdmin);
            WantsToBeContributor = bool.Parse(wantsToBeContributor); 
        }
    }
}
