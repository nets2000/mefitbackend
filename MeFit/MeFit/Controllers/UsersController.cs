﻿using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.Exercise;
using MeFit.Models.DTO.Goal;
using MeFit.Models.DTO.Profile;
using MeFit.Models.DTO.ProfilePic;
using MeFit.Models.DTO.User;
using MeFit.Models.DTO.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace MeFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly FitnessDbContext _context;
        private readonly IMapper _mapper;
        readonly string NO_USERS = "No users in database";
        public UsersController(FitnessDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of all users in the database
        /// </summary>
        /// <returns>A list of all of the users in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllUsers()
        {
            if (_context.Users == null)
            {
                return NotFound(NO_USERS);
            }

            List<User> users = await _context.Users.ToListAsync();
            List<UserReadDTO> userReadDTOs = _mapper.Map<List<UserReadDTO>>(users);

            return Ok(userReadDTOs);
        }

        /// <summary>
        /// Gets a users based on the given id
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns>The user with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserReadDTO>> GetUserById(int id)
        {
            if (_context.Users == null)
            {
                return NotFound(NO_USERS);
            }
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound($"{NO_USERS} with id {id}");
            }

            var userDTO = _mapper.Map<UserReadDTO>(user);

            return Ok(userDTO);
        }

        /// <summary>
        /// Gets a users current goal based on the given id
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns>The current goal for user with given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/goal")]
        public async Task<ActionResult<GoalReadDTO>> GetUserGoalById(string id)
        {
            if (_context.Users == null)
            {
                return NotFound(NO_USERS);
            }
            var user = await _context.Users.Where(u => u.UserId == id).Select(p => p.Id).FirstOrDefaultAsync();
            var profile = await _context.Profiles.Where(p => p.UserId == user).FirstOrDefaultAsync();
            if (user == default || profile == null)
            {
                return NotFound($"{NO_USERS} with id {id}");
            }
            var currentGoalId = await _context.Goals.Where(g => g.ProfileId == profile.Id && g.Current).FirstOrDefaultAsync();
            if (currentGoalId == null)
            {
                return NotFound($"No goals found for user with id {id}");
            }
            var goal = await _context.Goals.FindAsync(currentGoalId);
            if (goal == null)
            {
                return NotFound($"No goal in database with id {currentGoalId}");
            }
            var goalDTO = _mapper.Map<GoalReadDTO>(goal);

            return Ok(goalDTO);
        }

        /// <summary>
        /// Gets a profile based on the given user id
        /// </summary>
        /// <param name="id">The user id of the user</param>
        /// <returns>The profile of the given user if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/profile")]
        public async Task<ActionResult<ProfileReadDTO>> GetProfileByUserId(string id)
        {
            var user = await _context.Users.Where(user => user.UserId == id).FirstOrDefaultAsync();

            if (user == null)
            {
                return NotFound($"{NO_USERS} with id {id}");
            }
            var profile = await _context.Profiles.Where(profile => profile.UserId == user.Id).FirstOrDefaultAsync();
            if (profile == null)
            {
                return NotFound($"{NO_USERS} with id {id}");
            }

            var profileDTO = _mapper.Map<ProfileReadDTO>(profile);

            return Ok(profileDTO);
        }


        /// <summary>
        /// Gets a user based on the given user id
        /// </summary>
        /// <param name="id">The user id of the user</param>
        /// <returns>The user if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/userId")]
        public async Task<ActionResult<UserReadDTO>> GetUserByUserId(string id)
        {
            var user = await _context.Users.Where(user => user.UserId == id).FirstOrDefaultAsync();

            if (user == null)
            {
                return NotFound($"{NO_USERS} with id {id}");
            }

            var userDTO = _mapper.Map<UserReadDTO>(user);

            return Ok(userDTO);
        }
        
        /// <summary>
        /// Gets the exercises a user created based on the given user id
        /// </summary>
        /// <param name="id">The user id of the user</param>
        /// <returns>The exercises the user created if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/exercises-created")]
        public async Task<ActionResult<ICollection<ExerciseReadDTO>>> GetCreatedExercisesOfUser(int id)
        {
            var exercises = await _context.Exercises.Where(exercise => exercise.UserId == id).ToListAsync();
            if (exercises == null)
            {
                return NotFound($"{NO_USERS} with id {id} that have any exercises created");
            }
            var exercisesDTO = _mapper.Map<List<ExerciseReadDTO>>(exercises);

            return Ok(exercisesDTO);
        }
        /// <summary>
        /// Gets the workouts a user created based on the given user id
        /// </summary>
        /// <param name="id">The user id of the user</param>
        /// <returns>The workouts the user created if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/workouts-created")]
        public async Task<ActionResult<ICollection<WorkoutReadDTO>>> GetCreatedWorkoutsOfUser(int id)
        {
            var workouts = await _context.Workouts.Where(workout => workout.UserId == id).ToListAsync();

            if (workouts == null)
            {
                return NotFound($"{NO_USERS} with id {id} that have any workouts created");
            }

            var workoutsDTO = _mapper.Map<List<WorkoutReadDTO>>(workouts);

            return Ok(workoutsDTO);
        }

        /// <summary>
        /// Gets a profile pic based on the given user id
        /// </summary>
        /// <param name="id">The user id of the user</param>
        /// <returns>The profile pic of the user if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/profilepic")]
        public async Task<ActionResult<ProfilePicReadDTO>> GetProfilePicOfUser(int id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound($"{NO_USERS} with id {id} found");
            }

            var profilePic = await _context.ProfilePics.Where(profilePic => profilePic.Id == user.ProfilePicId).FirstOrDefaultAsync();
            if (profilePic == null || profilePic == default)
            {
                return Ok(null);
            }

            var profilePicDTO = _mapper.Map<ProfilePicReadDTO>(profilePic);

            return Ok(profilePicDTO);
        }

        /// <summary>
        /// Updates a user with given ID to the given values
        /// </summary>
        /// <param name="id">The id of the user that needs to be updated</param>
        /// <param name="user">All the values required for the updated user</param>
        /// <returns>An action result that represents how the user update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody] UserUpdateDTO user)
        {
            if (!UserExists(id))
            {
                return NotFound($"{NO_USERS} with id {id}");
            }

            try
            {
                DTOUpdateToUser dto = new(user.FirstName, user.LastName, user.IsContributor, user.IsAdmin, user.WantsToBeContributor);

                var givenUser = await GetUserByIdDetached(id);
                var domainUser = _mapper.Map<User>(dto);
                domainUser.Id = givenUser.Id;
                domainUser.Password = givenUser.Password;
                domainUser.UserId = givenUser.UserId;
                domainUser.ProfilePicId = givenUser.ProfilePicId;

                _context.Entry(domainUser).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Updates the password of a user with given ID to the given password
        /// </summary>
        /// <param name="id">The id of the user that needs to be updated</param>
        /// <param name="user">The password for the updated user</param>
        /// <returns>An action result that represents how the user update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/update_password")]
        public async Task<IActionResult> UpdateUserPassword(int id, [FromBody] UserUpdatePasswordDTO user)
        {
            if (id != user.Id)
            {
                return BadRequest($"Request id {id} doesn't match body id {user.Id}");
            }

            if (!UserExists(id))
            {
                return NotFound($"{NO_USERS} with id {id}");
            }

            try
            {
                var currentUser = await _context.Users.FindAsync(id);

                _context.Entry(currentUser).CurrentValues.SetValues(user);
                _context.Entry(currentUser).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Update the profile pic of the user
        /// </summary>
        /// <param name="id">The id of the user that needs to be updated</param>
        /// <param name="profilePicId">The id of profile pic that the user needs to have</param>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/profilepic")]
        public async Task<ActionResult> UpdateProfilePicFromUser(int id, int? profilePicId)
        {
            User selectedUser = await _context.Users.FindAsync(id);

            if (selectedUser == null)
            {
                return NotFound($"{NO_USERS} with id {id}");
            }

            try
            {
                selectedUser.ProfilePicId = profilePicId;
                _context.Entry(selectedUser).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new user to the database
        /// </summary>
        /// <param name="user">The values for the new user that needs to be added</param>
        /// <returns>An action result that represents how the user adding went</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<UserReadDTO>> CreateUser([FromBody] UserCreateDTO user)
        {
            DTOCreateToUser dto = new(user.Password, user.FirstName, user.LastName, user.IsContributor, user.IsAdmin, user.UserId,
                user.WantsToBeContributor);

            if (user.ProfilePicId != null)
            {
                dto.ProfilePicId = user.ProfilePicId;
            }

            var domainUser = _mapper.Map<User>(dto);

            try
            {
                _context.Users.Add(domainUser);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("POST request failed because of an internal server error");
            }

            var readUserDTO = _mapper.Map<UserReadDTO>(domainUser);
            return CreatedAtAction("GetUserById", new { id = readUserDTO.Id }, readUserDTO);
        }

        /// <summary>
        /// Removes a user from the database with given ID
        /// </summary>
        /// <param name="id">The ID for the user that needs to be deleted</param>
        /// <returns>An action result that represents how the user deletion went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound($"{NO_USERS} with id {id}");
            }
            try
            {
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("DELETE request failed because of an internal server error");
            }
            return NoContent();
        }

        /// <summary>
        /// Check whether a user exists
        /// </summary>
        /// <param name="id">The id that need to be checked</param>
        /// <returns>True if the user exists else it returns false</returns>
        [NonAction]
        private bool UserExists(int id)
        {
            return (_context.Users?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Get a user detached
        /// </summary>
        /// <param name="id">The id of the user that needs to be gotten</param>
        /// <returns>A user from the database with given ID</returns>
        [NonAction]
        private async Task<User> GetUserByIdDetached(int id)
        {
            var user = await _context.Users.FindAsync(id);
            _context.Entry(user).State = EntityState.Detached;
            return user;
        }
    }
}
