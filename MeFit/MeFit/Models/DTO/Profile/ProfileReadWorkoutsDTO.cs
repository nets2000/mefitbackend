﻿namespace MeFit.Models.DTO.Profile
{
    public class ProfileWorkoutReadDTO
    {
        public int Id { get; set; }

        public ICollection<Workout.WorkoutGoalReadDTO>? Workouts { get; set; }
    }
}
