﻿namespace MeFit.Models.DTO.Workout
{
    public class WorkoutGoalReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Complete { get; set; }
    }
}
