using MeFit.Models.Domain;

namespace MeFit.Data;

public class SeedExercises
{
    public static List<Exercise> GetSeedingExercises()
    {
        List<Exercise> exerciseSeeds = new List<Exercise>()
        {
            new Exercise
            {
                Id = 1, Name = "Barbel Hip Thrust",
                Description =
                    "A lower-body strength training exercise defined by lifting your lower back and torso with your knees bent and your upper body resting on a bench",
                TotalSets = 4, MinReps = 6, MaxReps = 8,
                Image = "", VidLink = "https://youtu.be/un4sFDkGlVY"
            },
            new Exercise
            {
                Id = 2, Name = "Hyperextension for Glutes", Description = "", TotalSets = 3, MinReps = 8, MaxReps = 10,
                Image = "",
                VidLink = "https://youtu.be/IU-PJkLoHio"
            },
            new Exercise
            {
                Id = 3, Name = "Barbell Deadlift",
                Description =
                    "A loaded barbell is lifted off the ground to the level of the hips, torso perpendicular to the floor, before being placed back on the ground",
                TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "", VidLink = "https://youtu.be/234EbYt2U7c"
            },
            new Exercise
            {
                Id = 4, Name = "Lying Leg Curl", Description = "", TotalSets = 3, MinReps = 12, MaxReps = 15,
                Image = "", VidLink = "https://youtu.be/GLbTloEDiNM"
            },
            new Exercise
            {
                Id = 5, Name = "Leg Extensions", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://youtu.be/5UBkMcdFDjU"
            },
            new Exercise
            {
                Id = 6, Name = "Ab Wheel Rollout", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/m6cYbFjP-U4"
            },
            new Exercise
            {
                Id = 7, Name = "Wide grip pull up", Description = "", TotalSets = 4, MinReps = 6, MaxReps = 8,
                Image = "",
                VidLink = "https://youtu.be/2RcE90XDK3Q"
            },
            new Exercise
            {
                Id = 8, Name = "Seated cable row", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/6UEvt8KXyQY"
            },
            new Exercise
            {
                Id = 9, Name = "Seated dumbbell shoulder press", Description = "", TotalSets = 3, MinReps = 8,
                MaxReps = 10, Image = "",
                VidLink = "https://youtu.be/QFxBA43GUFM"
            },
            new Exercise
            {
                Id = 10, Name = "Seated dumbbell side raise", Description = "", TotalSets = 3, MinReps = 10,
                MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/u88uvszRoZI"
            },
            new Exercise
            {
                Id = 11, Name = "Bent over dumbbell lateral raise", Description = "", TotalSets = 3, MinReps = 12,
                MaxReps = 15, Image = "",
                VidLink = "https://youtu.be/dN1k-MWLHT4"
            },
            new Exercise
            {
                Id = 12, Name = "Incline dumbbell curl", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://youtu.be/XChhbTXx7hY"
            },
            new Exercise
            {
                Id = 13, Name = "Dumbbell kickback", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/PBFqCPI85nw"
            },
            new Exercise
            {
                Id = 14, Name = "Back squat", Description = "", TotalSets = 4, MinReps = 10, MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/b1OVoeazIJY"
            },
            new Exercise
            {
                Id = 15, Name = "Bulgarian split squat", Description = "", TotalSets = 3, MinReps = 12, MaxReps = 15,
                Image = "",
                VidLink = "https://youtu.be/W8X6SroCQC0"
            },
            new Exercise
            {
                Id = 16, Name = "Machine hip abduction", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://youtu.be/tl7rVxWrEFw"
            },
            new Exercise
            {
                Id = 17, Name = "Incline sit ups", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/gtXGLp3llTw"
            },
            new Exercise
            {
                Id = 18, Name = "Romanian deadlift", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://youtu.be/m8phGf0SN_Q"
            },
            new Exercise
            {
                Id = 19, Name = "Walking dumbbell lunges", Description = "", TotalSets = 3, Reps = 15, Image = "",
                VidLink = "https://youtu.be/5fqt7SZBz8I"
            },
            new Exercise
            {
                Id = 20, Name = "Standing calf raise", Description = "", TotalSets = 2, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/lSmvqCevJe8"
            },
            new Exercise
            {
                Id = 21, Name = "Reverse crunch", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://www.youtube.com/watch?v=ujTWhn2Mvsc"
            },
            new Exercise
            {
                Id = 22, Name = "Front lat pulldown", Description = "", TotalSets = 3, MinReps = 8, MaxReps = 10,
                Image = "",
                VidLink = "https://youtu.be/MgP3V0LCLjM"
            },
            new Exercise
            {
                Id = 23, Name = "Seated dumbbell shoulder press", Description = "", TotalSets = 3, MinReps = 8,
                MaxReps = 10, Image = "",
                VidLink = "https://youtu.be/QFxBA43GUFM"
            },
            new Exercise
            {
                Id = 24, Name = "One arm dumbbell row", Description = "", TotalSets = 3, Reps = 12, Image = "",
                VidLink = "https://youtu.be/DT_Ui3qPllY"
            },
            new Exercise
            {
                Id = 25, Name = "Machine preacher curl", Description = "", TotalSets = 2, MinReps = 8, MaxReps = 10,
                Image = "",
                VidLink = "https://youtu.be/xwNr7icqFW8"
            },
            new Exercise
            {
                Id = 26, Name = "Pronated cable fly", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/EzleutBmCAQ"
            },
            new Exercise
            {
                Id = 27, Name = "Cable triceps pushdown ", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/bYlgQgAiWBA"
            },
            new Exercise
            {
                Id = 28, Name = "Weighted sit ups", Description = "", TotalSets = 3, Reps = 15, Image = "",
                VidLink = "https://youtu.be/vlMbsyQGSD4"
            },
            new Exercise
                { Id = 29, Name = "Leg raises", Description = "", TotalSets = 3, Reps = 15, Image = "", VidLink = "" },
            new Exercise
            {
                Id = 30, Name = "Dip assisted", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12, Image = "",
                VidLink = "https://www.youtube.com/watch?v=8zFcJWD9Y_E"
            },
            new Exercise
            {
                Id = 31, Name = "Standing military press ", Description = "", TotalSets = 3, MinReps = 8, MaxReps = 10,
                Image = "",
                VidLink = "https://youtu.be/sMZR0LoFutc"
            },
            new Exercise
            {
                Id = 32, Name = "Hyper extensions", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://youtu.be/XHPP3uySPPA"
            },
            new Exercise
            {
                Id = 33, Name = "Tuck up", Description = "", Image = "", TotalSets = 3, MinReps = 10, MaxReps = 15,
                VidLink = "https://youtu.be/qjVXzb3bRjs"
            },
            new Exercise
            {
                Id = 34, Name = "Assisted pull up", Description = "", TotalSets = 4, MinReps = 6, MaxReps = 8,
                Image = "",
                VidLink = "https://youtu.be/4DgVajQapJs"
            },
            new Exercise
            {
                Id = 35, Name = "Seated dumbell curl", Description = "", TotalSets = 3, MinReps = 12, MaxReps = 15,
                Image = "",
                VidLink = "https://youtu.be/CY2Uz5ZQyiM"
            },
            new Exercise
            {
                Id = 36, Name = "Overhead rope triceps extension", Description = "", TotalSets = 3, MinReps = 12,
                MaxReps = 15, Image = "",
                VidLink = "https://youtu.be/UKbLvRZMyeM"
            },
            new Exercise
            {
                Id = 37, Name = "Incline dumbbell chest press", Description = "", TotalSets = 3, MinReps = 15,
                MaxReps = 20, Image = "",
                VidLink = "https://youtu.be/BV1WgsasJU4"
            },
            new Exercise
            {
                Id = 38, Name = "Reverse dumbbell fly", Description = "", TotalSets = 3, MinReps = 12, MaxReps = 15,
                Image = "",
                VidLink = "https://youtu.be/TW3z7DxBeq4"
            },
            new Exercise
            {
                Id = 39, Name = "Hanging knee raises", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://youtu.be/hT3wq3xWZsc"
            },
            new Exercise
            {
                Id = 40, Name = "45 graden leg press (feet low)", Description = "", TotalSets = 3, MinReps = 10,
                MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/WDMwQiPk6Gw"
            },
            new Exercise
            {
                Id = 41, Name = "Romanian deadlift", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/m8phGf0SN_Q"
            },
            new Exercise
            {
                Id = 42, Name = "Cable kickback", Description = "", TotalSets = 3, MinReps = 12, MaxReps = 15,
                Image = "",
                VidLink = "https://youtu.be/FfvGyH5ugA0"
            },
            new Exercise
            {
                Id = 43, Name = "Negative pull ups", Description = "", TotalSets = 4, MinReps = 6, MaxReps = 8,
                Image = "",
                VidLink = "https://youtu.be/gbPURTSxQLY"
            },
            new Exercise
            {
                Id = 44, Name = "Standing dumbbell shoulder press", Description = "", TotalSets = 3, MinReps = 12,
                MaxReps = 15, Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 45, Name = "Standing dumbbell side raise", Description = "", TotalSets = 3, MinReps = 12,
                MaxReps = 15, Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 46, Name = "Biceps rope hammer curl", Description = "", TotalSets = 3, MinReps = 12, MaxReps = 15,
                Image = "",
                VidLink = "https://youtu.be/hxqZ5FLqA1A"
            },
            new Exercise
            {
                Id = 47, Name = "One arm cable triceps kickback", Description = "", TotalSets = 3, MinReps = 12,
                MaxReps = 15, Image = "",
                VidLink = "https://youtu.be/Brrg3OMeePA"
            },
            new Exercise
            {
                Id = 48, Name = "Machine rear delt fly", Description = "", TotalSets = 3, MinReps = 15, MaxReps = 20,
                Image = "",
                VidLink = "https://youtu.be/aW5pIf2LRbw"
            },
            new Exercise
            {
                Id = 49, Name = "Swiss ball V ups", Description = "", TotalSets = 3, Reps = 20, Image = "",
                VidLink = "https://youtu.be/Jbk7DfnaKXs"
            },
            new Exercise
            {
                Id = 50, Name = "Reverse barbell lunge", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/R-g5yPNYv2k"
            },
            new Exercise
            {
                Id = 51, Name = "Incline machine chest press", Description = "", TotalSets = 3, MinReps = 10,
                MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/hdAkAv3iE00"
            },
            new Exercise
            {
                Id = 52, Name = "Dumbbell shoulder press", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/m4Erm0S3n-w"
            },
            new Exercise
            {
                Id = 53, Name = "Cross cable triceps extensions", Description = "", TotalSets = 3, MinReps = 15,
                MaxReps = 20, Image = "",
                VidLink = "https://youtu.be/OEYwFdaqwRM"
            },
            new Exercise
            {
                Id = 54, Name = "Power squat", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/r7nEuWVgl6s"
            },
            new Exercise
            {
                Id = 55, Name = "Flat dumbbell chest press", Description = "", TotalSets = 3, MinReps = 10,
                MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/h_q2PiVzdSM"
            },
            new Exercise
            {
                Id = 56, Name = "Seated leg curls", Description = "", TotalSets = 3, Reps = 15, Image = "",
                VidLink = "https://www.youtube.com/watch?v=A4hJynJoWUo"
            },
            new Exercise
            {
                Id = 57, Name = "Kickstand romanian deadlift", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Description = "", Image = "",
                VidLink = "https://youtu.be/RrLgwECd1C0"
            },
            new Exercise
            {
                Id = 58, Name = "Single leg glute bridge", Description = "", TotalSets = 3, Reps = 15, Image = "",
                VidLink = "https://youtu.be/zbOBWOcvtRU"
            },
            new Exercise
            {
                Id = 59, Name = "Wide stance barbell squat", Description = "", TotalSets = 3, MinReps = 10,
                MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/oddwOgfxZOc"
            },
            new Exercise
            {
                Id = 60, Name = "Rowing machine circular", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/d5D14-3pdnw"
            },
            new Exercise
            {
                Id = 61, Name = "Hyper extension for lower back", Description = "", TotalSets = 3, MinReps = 10,
                MaxReps = 12, Image = "",
                VidLink = "https://youtu.be/XkHNUBjCwuw"
            },
            new Exercise
            {
                Id = 62, Name = "Dumbbell spider curl", Description = "", TotalSets = 3, MinReps = 10, MaxReps = 12,
                Image = "",
                VidLink = "https://youtu.be/MJuyjjfnoBo"
            },
            new Exercise
            {
                Id = 63, Name = "Rowing warmup", Description = "2 km rowing in 10 minutes", Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 64, Name = "Crosstrainer", Description = "20 minutes on crosstrainer", Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 65, Name = "Cycling warmup", Description = "4,5 km cycling in 10 minutes", Image = "",
                VidLink = ""
            },
            new Exercise { Id = 66, Name = "Jogging", Description = "Jog 3 x 400 meter", Image = "", VidLink = "" },
            new Exercise
            {
                Id = 67, Name = "Cardio warmup",
                Description = "Choose a cardio machine and burn 100 kcal in 10 minutes", Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 68, Name = "Rowing warmup", Description = "1 km rowing in 5 minutes", Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 69, Name = "Cardio warmup",
                Description = "Choose a cardio machine and burn 50 kcal in 5 minutes", Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 70, Name = "Cardio warmup", Description = "Choose a cardio machine for 3 minutes", Image = "",
                VidLink = ""
            },
            new Exercise
                { Id = 71, Name = "Rowing warmup", Description = "1200 meter rowing", Image = "", VidLink = "" },
            new Exercise
            {
                Id = 72, Name = "Crosstrainer", Description = "30 minutes on crosstrainer", Image = "", VidLink = ""
            },
            new Exercise
            {
                Id = 73, Name = "Stairclimber", Description = "15 minutes on stairclimber", Image = "", VidLink = ""
            },
            new Exercise { Id = 74, Name = "Jogging", Description = "Jog for 6 minutes", Image = "", VidLink = "" },
            new Exercise
            {
                Id = 75, Name = "Half sit up", Description = "", TotalSets = 3, MinReps = 20, MaxReps = 25, Image = "",
                VidLink = ""
            },
            new Exercise
            {
                Id = 76, Name = "Reverse crunch & leg raise", Description = "", TotalSets = 3, MinReps = 20,
                MaxReps = 25, Image = "", VidLink = ""
            }

        };
        return exerciseSeeds;
    }
}