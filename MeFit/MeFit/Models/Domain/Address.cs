﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class Address
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "Address line 1 contains too many characters. Max lenght is: 100")]
        public string AddressLine1 { get; set; }

        [MaxLength(100, ErrorMessage = "Address line 2 contains too many characters. Max lenght is: 100")]
        public string AddressLine2 { get; set; }

        [MaxLength(100, ErrorMessage = "Address line 3 contains too many characters. Max lenght is: 100")]
        public string AddressLine3 { get; set; }

        [MaxLength(10, ErrorMessage = "Postal code contains too many characters. Max length is: 10")]
        public string PostalCode { get; set; }

        [MaxLength(100, ErrorMessage = "City contains too many characters. Max length is: 100")]
        public string City { get; set; }

        [MaxLength(100, ErrorMessage = "Country contains too many characters. Max length is: 100")]
        public string Country { get; set; }

        public ICollection<Profile> Profiles { get; set; }
    }
}
