﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.FitnessProgram
{
    public class FitnessProgramUpdateDTO
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Category contains too many characters. Max length is: 100")]
        public string Category { get; set; }
    }
}
