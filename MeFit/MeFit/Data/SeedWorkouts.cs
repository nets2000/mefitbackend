using MeFit.Models.Domain;

namespace MeFit.Data;

public class SeedWorkouts
{
    public static List<Workout> GetSeedingWorkouts()
    {
        List<Workout> workoutsSeed = new List<Workout>()
        {
            new Workout { Id = 1, Name = "Workout 1", Type = "Lower body"},
            new Workout { Id = 2, Name = "Workout 2", Type = "Upper body"},
            new Workout { Id = 3, Name = "Workout 3", Type = "Lower body"},
            new Workout { Id = 4, Name = "Workout 4", Type = "Lower body"},
            new Workout { Id = 5, Name = "Workout 5", Type = "Upper body"},
            new Workout { Id = 6, Name = "Workout 6", Type = "Full body"}
        };
        return workoutsSeed;
    }
}