﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.User
{
    public class UserCreateDTO
    {
        [MaxLength(1000, ErrorMessage = "Password contains too many characters. Max length is: 1000")]
        public string Password { get; set; }

        [MaxLength(100, ErrorMessage = "First name contains too many characters. Max length is: 100")]
        public string FirstName { get; set; }

        [MaxLength(100, ErrorMessage = "Last name contains too many characters. Max length is: 100")]
        public string LastName { get; set; }

        [MaxLength(300, ErrorMessage = "User ID contains too many characters. Max length is: 300")]
        public string UserId { get; set; }

        [MaxLength(5, ErrorMessage = "Wants to be contributor contains too many characters. Max length is: 5")]
        public string WantsToBeContributor { get; set; }

        [MaxLength(5, ErrorMessage = "Is contributor contains too many characters. Max length is: 5")]
        public string IsContributor { get; set; }

        [MaxLength(5, ErrorMessage = "Is admin contains too many characters. Max length is: 5")]
        public string IsAdmin { get; set; }

        public int? ProfilePicId { get; set; }
    }
}
