﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class addeduseridtousertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Users",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Program 1");

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Program 2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Users");

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Workout 1");

            migrationBuilder.UpdateData(
                table: "FitnessPrograms",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Workout 2");
        }
    }
}
