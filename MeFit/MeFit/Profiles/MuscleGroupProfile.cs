﻿using MeFit.Models.Domain;
using MeFit.Models.DTO.MuscleGroup;

namespace MeFit.Profiles
{
    public class MuscleGroupProfile : AutoMapper.Profile
    {
        public MuscleGroupProfile()
        {
            CreateMap<MuscleGroup, MuscleGroupReadDTO>();
            CreateMap<MuscleGroup, MuscleGroupExerciseReadDTO>()
                .ForMember(readDTO => readDTO.ExerciseIds,
                opt => opt.MapFrom(muscleGroup => muscleGroup.Exercises
                .Select(exercise => exercise.Id)
                .ToList<int>()));
            CreateMap<MuscleGroupUpdateDTO, MuscleGroup>();
            CreateMap<MuscleGroupCreateDTO, MuscleGroup>();
        }
    }
}
