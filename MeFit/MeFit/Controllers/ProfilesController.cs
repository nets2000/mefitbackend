﻿using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.FitnessProgram;
using MeFit.Models.DTO.Goal;
using MeFit.Models.DTO.Profile;
using MeFit.Models.DTO.User;
using MeFit.Models.DTO.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProfilesController : ControllerBase
    {
        private readonly FitnessDbContext _context;
        private readonly IMapper _mapper;
        readonly string NO_PROFILES = "No profiles in database";
        public ProfilesController(FitnessDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of all profiles in the database
        /// </summary>
        /// <returns>A list of all of the profiles in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProfileReadDTO>>> GetAllProfiles()
        {
            if (_context.Profiles == null)
            {
                return NotFound();
            }

            List<Models.Domain.Profile> profiles = await _context.Profiles.ToListAsync();
            List<ProfileReadDTO> profileReadDTOs = _mapper.Map<List<ProfileReadDTO>>(profiles);

            return Ok(profileReadDTOs);
        }

        /// <summary>
        /// Gets a profile based on the given id
        /// </summary>
        /// <param name="id">The id of the profile</param>
        /// <returns>The profile with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ProfileReadDTO>> GetProfileById(int id)
        {
            if (_context.Profiles == null)
            {
                return NotFound(NO_PROFILES);
            }

            var profile = await _context.Profiles.FindAsync(id);

            if (profile == null)
            {
                return NotFound($"{NO_PROFILES} in database with id {id}");
            }

            var profileDTO = _mapper.Map<ProfileReadDTO>(profile);

            return Ok(profileDTO);
        }

        /// <summary>
        /// Gets all the current goals from a profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to show the current goals</param>
        /// <returns>A list of current goals that are in the profile</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/goals")]
        public async Task<ActionResult<IEnumerable<GoalReadDTO>>> GetGoalsFromProfileById(int id)
        {
            Models.Domain.Profile profile = await _context.Profiles.FindAsync(id);
            if (profile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }
            var goals = await _context.Goals
                .Where(g => g.ProfileId == profile.Id && g.Current).ToListAsync();
            if (goals == null)
            {
                return NotFound($"No goals in database for profile with id {id}"); ;
            }
            return Ok(_mapper.Map<List<Goal>, IEnumerable<GoalReadDTO>>(goals));
        }

        /// <summary>
        /// Gets all the goal history in a profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to show the goal history</param>
        /// <returns>A list of past goals that are in the profile</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/goals_history")]
        public async Task<ActionResult<List<GoalReadDTO>>> GetPastGoalsFromProfileById(int id)
        {
            Models.Domain.Profile profile = await _context.Profiles.Where(p => p.Id == id).FirstOrDefaultAsync();
            if (profile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }
            var goals = await _context.Goals.Where(g => g.ProfileId == id && !g.Current).ToListAsync();
            if (goals == null)
            {
                return NotFound($"No goals in database for profile with id {id}");
            }
            return Ok(_mapper.Map<List<Goal>, List<GoalReadDTO>>(goals));
        }

        /// <summary>
        /// Gets all the workouts in a profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to show all the workouts</param>
        /// <returns>A list of workouts that are in the profile</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/workouts")]
        public async Task<ActionResult<IEnumerable<WorkoutGoalReadDTO>>> GetAllWorkOutsFromProfileById(int id)
        {
            var goals = await _context.Goals
                .Where(g => g.ProfileId == id)
                .ToListAsync();
            if (goals == null)
            {
                return NotFound($"No workouts in database for profile with id {id}");
            }
            List<int> workoutIds = new();
            foreach (var goal in goals)
            {
                var goalWorkouts = await _context.GoalWorkout.Where(gw => gw.GoalId == goal.Id).Select(gw => gw.WorkoutId).ToListAsync();
                if (goalWorkouts != null) workoutIds.AddRange(goalWorkouts);
            }
            var workouts = await GetWorkoutListFromIds(workoutIds);
            List<WorkoutGoalReadDTO> dtos = new List<WorkoutGoalReadDTO>();
            foreach (var w in workouts)
            {
                dtos.Add(new WorkoutGoalReadDTO()
                {
                    Id = w.Id,
                    Name = w.Name,
                    Type = w.Type,
                    Complete = _context.GoalWorkout.Where(pw => pw.Goal.ProfileId == id && pw.WorkoutId == w.Id).Select(pw => pw.Complete).First()
                });
            }

            return Ok(dtos);
        }

        /// <summary>
        /// Gets all the programs in a profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to show all the programs</param>
        /// <returns>A list of programs that are in the profile</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/programs")]
        public async Task<ActionResult<IEnumerable<FitnessProgramReadDTO>>> GetAllProgramsFromProfileById(int id)
        {
            Models.Domain.Profile profile = await _context.Profiles
                .Include(p => p.FitnessPrograms)
                    .Where(p => p.Id == id).FirstOrDefaultAsync();
            if (profile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }

            ProfileReadProgramsDTO dto = _mapper.Map<ProfileReadProgramsDTO>(profile);

            var programs = await GetProgramListFromIds(dto.FitnessProgramIds.ToList());
            List<FitnessProgramReadDTO> programDTOs = _mapper.Map<List<FitnessProgramReadDTO>>(programs);

            return Ok(programDTOs);
        }

        /// <summary>
        /// Updates a profile with given ID to the given values
        /// </summary>
        /// <param name="id">The id of the profile that needs to be updated</param>
        /// <param name="profile">All the values required for the updated profile</param>
        /// <returns>An action result that represents how the profile update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProfile(int id, [FromBody] ProfileUpdateDTO profile)
        {
            if (!ProfileExists(id))
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }

            var profileById = await _context.Profiles.FindAsync(id);

            try
            {
                _context.Entry(profileById).CurrentValues.SetValues(profile);
                _context.Entry(profileById).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Update the programs of the profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to be updated</param>
        /// <param name="programIds">The ids of programs that the profile needs to have</param>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/programs")]
        public async Task<ActionResult> UpdateProgramsFromProfile(int id, List<int> programIds)
        {
            Models.Domain.Profile selectedProfile = await _context.Profiles.Include(p => p.FitnessPrograms)
                .Where(p => p.Id == id).FirstOrDefaultAsync();

            if (selectedProfile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }

            try
            {
                List<FitnessProgram> selectedPrograms = await GetProgramListFromIds(programIds);

                selectedProfile.FitnessPrograms = selectedPrograms;
                _context.Entry(selectedProfile).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Update the goal of the profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to be updated</param>
        /// <param name="goalID">The id of goal that the profile needs to have</param>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/goal")]
        public async Task<ActionResult> UpdateGoalFromProfile(int id, int goalID)
        {
            Models.Domain.Profile selectedProfile = await _context.Profiles.FindAsync(id);

            if (selectedProfile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }

            try
            {
                var goal = _context.Goals.Find(goalID);
                if (goal == null)
                {
                    return NotFound($"No goals found for profile with id {id}");
                }
                if (selectedProfile.Goals == null)
                    selectedProfile.Goals = new List<Goal> { goal };
                else
                    selectedProfile.Goals.Add(goal);
                _context.Entry(selectedProfile).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }


        /// <summary>
        /// Update the adress of the profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to be updated</param>
        /// <param name="addressId">The id of adress that the profile needs to have</param>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/address")]
        public async Task<ActionResult> UpdateAddressFromProfile(int id, int addressId)
        {
            Models.Domain.Profile selectedProfile = await _context.Profiles.FindAsync(id);

            if (selectedProfile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }

            try
            {
                selectedProfile.AddressId = addressId;
                _context.Entry(selectedProfile).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Update the user of the profile
        /// </summary>
        /// <param name="id">The id of the profile that needs to be updated</param>
        /// <param name="userId">The id of user that the profile needs to have</param>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/user")]
        public async Task<ActionResult> UpdateUserFromProfile(int id, int userId)
        {
            Models.Domain.Profile selectedProfile = await _context.Profiles.FindAsync(id);

            if (selectedProfile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }

            try
            {
                selectedProfile.UserId = userId;
                _context.Entry(selectedProfile).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new profile to the database
        /// </summary>
        /// <param name="profile">The values for the new profile that needs to be added</param>
        /// <returns>An action result that represents how the profile adding went</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<ProfileReadDTO>> AddProfile([FromBody] ProfileCreateDTO profile)
        {
            var domainProfile = _mapper.Map<Models.Domain.Profile>(profile);
            try
            {
                _context.Profiles.Add(domainProfile);

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                //throw;
                return Problem($"POST request failed because of an internal server error: {e.Message}");
            }

            var readProfileDTO = _mapper.Map<ProfileReadDTO>(domainProfile);

            return CreatedAtAction("GetProfileById", new { Id = readProfileDTO.Id }, readProfileDTO);
        }

        /// <summary>
        /// Removes a profile from the database with given ID
        /// </summary>
        /// <param name="id">The ID for the profile that needs to be deleted</param>
        /// <returns>An action result that represents how the profile deletion went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfile(int id)
        {
            var profile = await _context.Profiles.FindAsync(id);
            if (profile == null)
            {
                return NotFound($"{NO_PROFILES} with id {id}");
            }

            try
            {
                _context.Profiles.Remove(profile);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("DELETE request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Check whether a profile exists
        /// </summary>
        /// <param name="id">The id that need to be checked</param>
        /// <returns>True if the profile exists else it returns false</returns>
        [NonAction]
        private bool ProfileExists(int id)
        {
            return (_context.Profiles?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Get all the workouts with the given ids
        /// </summary>
        /// <param name="workoutIds">A list of workout ids</param>
        /// <returns>A list of workouts based on the given ids</returns>
        [NonAction]
        private async Task<List<Workout>> GetWorkoutListFromIds(List<int> workoutIds)
        {
            List<Workout> selectedWorkouts = new List<Workout>();

            for (int i = 0; i < workoutIds.Count; i++)
            {
                selectedWorkouts.Add(await _context.Workouts.FindAsync(workoutIds[i]));
            }

            return selectedWorkouts;
        }

        /// <summary>
        /// Get all the programs with the given ids
        /// </summary>
        /// <param name="programIds">A list of program ids</param>
        /// <returns>A list of programs based on the given ids</returns>
        [NonAction]
        private async Task<List<FitnessProgram>> GetProgramListFromIds(List<int> programIds)
        {
            List<FitnessProgram> selectedPrograms = new List<FitnessProgram>();

            for (int i = 0; i < programIds.Count; i++)
            {
                selectedPrograms.Add(await _context.FitnessPrograms.FindAsync(programIds[i]));
            }

            return selectedPrograms;
        }
    }
}
