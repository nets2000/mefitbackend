using MeFit.Models.Domain;
using MeFit.Models.DTO.Exercise;
using Profile = AutoMapper.Profile;

namespace MeFit.Profiles;

public class ExerciseProfile : Profile
{
    public ExerciseProfile()
    {
        CreateMap<Exercise, ExerciseReadDTO>()
            .ForMember(erdto => erdto.WorkoutIds,
                opt => opt
                .MapFrom(e => e.Workouts
                    .Select(w => w.Id)
                    .ToList()))
            .ForMember(erdto => erdto.MuscleGroupIds,
                opt => opt
                    .MapFrom(e => e.MuscleGroups
                        .Select(m => m.Id)
                        .ToList()));
        CreateMap<ExerciseCreateDTO, Exercise>();
        CreateMap<ExerciseUpdateDTO, Exercise>();
    }
}