﻿using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.Address;
using MeFit.Models.DTO.Adress;
using MeFit.Models.DTO.Profile;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AddressesController : ControllerBase
    {
        private readonly FitnessDbContext _context;
        private readonly IMapper _mapper;
        readonly string NO_ADDRESSES = "No addresses in database";
        public AddressesController(FitnessDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all addresses from the database
        /// </summary>
        /// <returns>A list of all addresses in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AddressReadDTO>>> GetAllAddresses()
        {
            if (_context.Addresses == null)
            {
                return NotFound(NO_ADDRESSES);
            }
            List<Address> addresses = await _context.Addresses.ToListAsync();
            List<AddressReadDTO> addressReadDTOs = _mapper.Map<List<AddressReadDTO>>(addresses);

            return Ok(addressReadDTOs);
        }

        /// <summary>
        /// Gets a address based on the given id
        /// </summary>
        /// <param name="id">The id of the address</param>
        /// <returns>The address with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<AddressReadDTO>> GetAddressById(int id)
        {
            if (_context.Addresses == null || !AddressExists(id))
            {
                return NotFound(NO_ADDRESSES);
            }
            var address = await _context.Addresses.FindAsync(id);

            if (address == null)
            {
                return NotFound($"{NO_ADDRESSES} + with id {id}");
            }

            var adressDTO = _mapper.Map<AddressReadDTO>(address);

            return Ok(adressDTO);
        }

        /// <summary>
        /// Gets all the profiles in a adress
        /// </summary>
        /// <param name="id">The id of the address that needs to show all the profiles</param>
        /// <returns>A list of profiles that are in the adress</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/profiles")]
        public async Task<ActionResult<IEnumerable<ProfileReadDTO>>> GetAllProfilesFromAddressById(int id)
        {
            Address address = await _context.Addresses.Include(a => a.Profiles).Where(p => p.Id == id).FirstOrDefaultAsync();
            if (address == null)
            {
                return NotFound(NO_ADDRESSES);
            }

            AddressReadProfilesDTO dto = _mapper.Map<AddressReadProfilesDTO>(address);

            var profiles = await GetProfileListFromIds(dto.ProfileIds.ToList());
            List<ProfileReadDTO> profileDTOs = _mapper.Map<List<ProfileReadDTO>>(profiles);

            return Ok(profileDTOs);
        }

        /// <summary>
        /// Gets a address based on the given data
        /// </summary>
        /// <param name="id">The postal code of the address</param>
        /// <param name="city">The city of the address</param>
        /// <param name="street">The street of the address</param>
        /// <param name="houseNumber">The house number of the address</param>
        /// <returns>The address based on given data if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/AdressByData")]
        public async Task<ActionResult<AddressReadDTO>> GetAddressByAdressData(string id, string city, string street, string houseNumber)
        {
            if (_context.Addresses == null)
            {
                return NotFound($"{NO_ADDRESSES}");
            }
            var address = await _context.Addresses.Where(address => address.PostalCode == id && address.AddressLine1 == street &&
                address.City == city && address.AddressLine2 == houseNumber).FirstOrDefaultAsync();

            if (address == null || address == default)
            {
                return NotFound($"{NO_ADDRESSES} with id {id}");
            }

            var adressDTO = _mapper.Map<AddressReadDTO>(address);

            return Ok(adressDTO);
        }

        /// <summary>
        /// Updates a address with given ID to the given values
        /// </summary>
        /// <param name="id">The id of the address that needs to be updated</param>
        /// <param name="address">All the values required for the updated address</param>
        /// <returns>An action result that represents how the address update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAddress(int id, [FromBody] AddressUpdateDTO address)
        {
            if (!AddressExists(id))
            {
                return NotFound($"{NO_ADDRESSES} + with id {id}");
            }

            var addressById = await _context.Addresses.FindAsync(id);

            try
            {
                _context.Entry(addressById).CurrentValues.SetValues(address);
                _context.Entry(addressById).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT Request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new address to the database
        /// </summary>
        /// <param name="address">The values for the new address that needs to be added</param>
        /// <returns>An action result that represents how the address adding went</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<AddressReadDTO>> AddAddress([FromBody] AddressCreateDTO address)
        {
            var domainAddress = _mapper.Map<Address>(address);

            try
            {
                _context.Addresses.Add(domainAddress);

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return Problem($"POST request failed because of an internal server error: {ex.InnerException.Message}");
            }

            var readAddressDTO = _mapper.Map<AddressReadDTO>(domainAddress);

            return CreatedAtAction("GetAddressById", new { Id = readAddressDTO.Id }, readAddressDTO);
        }

        /// <summary>
        /// Removes a address from the database with given ID
        /// </summary>
        /// <param name="id">The ID for the address that needs to be deleted</param>
        /// <returns>An action result that represents how the address deletion went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAddress(int id)
        {
            if (_context.Addresses == null)
            {
                return NotFound(NO_ADDRESSES);
            }
            var address = await _context.Addresses.FindAsync(id);
            if (address == null)
            {
                return NotFound($"{NO_ADDRESSES} + with id {id}");
            }

            _context.Addresses.Remove(address);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Check whether a address exists
        /// </summary>
        /// <param name="id">The id that need to be checked</param>
        /// <returns>True if the address exists else it returns false</returns>
        [NonAction]
        private bool AddressExists(int id)
        {
            return (_context.Addresses?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Get all the profiles with the given ids
        /// </summary>
        /// <param name="profileIds">A list of profile ids</param>
        /// <returns>A list of profiles based on the given ids</returns>
        [NonAction]
        private async Task<List<Models.Domain.Profile>> GetProfileListFromIds(List<int> profileIds)
        {
            List<Models.Domain.Profile> selectedProfiles = new List<Models.Domain.Profile>();

            for (int i = 0; i < profileIds.Count; i++)
            {
                selectedProfiles.Add(await _context.Profiles.FindAsync(profileIds[i]));
            }

            return selectedProfiles;
        }
    }
}
