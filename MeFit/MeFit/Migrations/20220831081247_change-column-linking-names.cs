﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class changecolumnlinkingnames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseMuscleGroup_Exercises_ExercisesId",
                table: "ExerciseMuscleGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseMuscleGroup_MuscleGroups_MuscleGroupsId",
                table: "ExerciseMuscleGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseWorkout_Exercises_ExercisesId",
                table: "ExerciseWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseWorkout_Workouts_WorkoutsId",
                table: "ExerciseWorkout");

            migrationBuilder.RenameColumn(
                name: "WorkoutsId",
                table: "ExerciseWorkout",
                newName: "WorkoutId");

            migrationBuilder.RenameColumn(
                name: "ExercisesId",
                table: "ExerciseWorkout",
                newName: "ExerciseId");

            migrationBuilder.RenameIndex(
                name: "IX_ExerciseWorkout_WorkoutsId",
                table: "ExerciseWorkout",
                newName: "IX_ExerciseWorkout_WorkoutId");

            migrationBuilder.RenameColumn(
                name: "ExercisesId",
                table: "ExerciseMuscleGroup",
                newName: "ExerciseId");

            migrationBuilder.RenameColumn(
                name: "MuscleGroupsId",
                table: "ExerciseMuscleGroup",
                newName: "MuscleGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ExerciseMuscleGroup_ExercisesId",
                table: "ExerciseMuscleGroup",
                newName: "IX_ExerciseMuscleGroup_ExerciseId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseMuscleGroup_Exercises_ExerciseId",
                table: "ExerciseMuscleGroup",
                column: "ExerciseId",
                principalTable: "Exercises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseMuscleGroup_MuscleGroups_MuscleGroupId",
                table: "ExerciseMuscleGroup",
                column: "MuscleGroupId",
                principalTable: "MuscleGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseWorkout_Exercises_ExerciseId",
                table: "ExerciseWorkout",
                column: "ExerciseId",
                principalTable: "Exercises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseWorkout_Workouts_WorkoutId",
                table: "ExerciseWorkout",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseMuscleGroup_Exercises_ExerciseId",
                table: "ExerciseMuscleGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseMuscleGroup_MuscleGroups_MuscleGroupId",
                table: "ExerciseMuscleGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseWorkout_Exercises_ExerciseId",
                table: "ExerciseWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_ExerciseWorkout_Workouts_WorkoutId",
                table: "ExerciseWorkout");

            migrationBuilder.RenameColumn(
                name: "WorkoutId",
                table: "ExerciseWorkout",
                newName: "WorkoutsId");

            migrationBuilder.RenameColumn(
                name: "ExerciseId",
                table: "ExerciseWorkout",
                newName: "ExercisesId");

            migrationBuilder.RenameIndex(
                name: "IX_ExerciseWorkout_WorkoutId",
                table: "ExerciseWorkout",
                newName: "IX_ExerciseWorkout_WorkoutsId");

            migrationBuilder.RenameColumn(
                name: "ExerciseId",
                table: "ExerciseMuscleGroup",
                newName: "ExercisesId");

            migrationBuilder.RenameColumn(
                name: "MuscleGroupId",
                table: "ExerciseMuscleGroup",
                newName: "MuscleGroupsId");

            migrationBuilder.RenameIndex(
                name: "IX_ExerciseMuscleGroup_ExerciseId",
                table: "ExerciseMuscleGroup",
                newName: "IX_ExerciseMuscleGroup_ExercisesId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseMuscleGroup_Exercises_ExercisesId",
                table: "ExerciseMuscleGroup",
                column: "ExercisesId",
                principalTable: "Exercises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseMuscleGroup_MuscleGroups_MuscleGroupsId",
                table: "ExerciseMuscleGroup",
                column: "MuscleGroupsId",
                principalTable: "MuscleGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseWorkout_Exercises_ExercisesId",
                table: "ExerciseWorkout",
                column: "ExercisesId",
                principalTable: "Exercises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExerciseWorkout_Workouts_WorkoutsId",
                table: "ExerciseWorkout",
                column: "WorkoutsId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
