﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class MuscleGroup
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        public ICollection<Exercise> Exercises { get; set; }
    }
}
