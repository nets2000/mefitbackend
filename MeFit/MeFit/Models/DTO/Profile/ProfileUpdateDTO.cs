﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.Profile
{
    public class ProfileUpdateDTO
    {
        public float Weight { get; set; }

        public float Height { get; set; }

        [MaxLength(1000, ErrorMessage = "Medical conditions contains too many characters. Max length is: 1000")]
        public string MedicalConditions { get; set; }

        [MaxLength(1000, ErrorMessage = "Disabilities contains too many characters. Max length is: 1000")]
        public string Disabilities { get; set; }

        [MaxLength(50, ErrorMessage = "Fitness evaluation contains too many characters. Max length is: 50")]
        public string FitnessEvaluation { get; set; }

        public ICollection<int> WorkoutIds { get; set; }

        public ICollection<int>? GoalIds { get; set; }
    }
}
