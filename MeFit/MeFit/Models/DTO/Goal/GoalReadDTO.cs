﻿namespace MeFit.Models.DTO.Goal
{
    public class GoalReadDTO
    {
        public int Id { get; set; }

        public DateTime EndDate { get; set; }

        public bool Achieved { get; set; }

        public bool Current { get; set; }

        public int? FitnessProgramId { get; set; }

    }
}
