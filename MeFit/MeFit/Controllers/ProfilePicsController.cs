﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MeFit.Data;
using MeFit.Models.Domain;
using AutoMapper;
using MeFit.Models.DTO.Profile;
using MeFit.Models.DTO.ProfilePic;
using Microsoft.AspNetCore.Authorization;

namespace MeFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProfilePicsController : ControllerBase
    {
        private readonly FitnessDbContext _context;
        private readonly IMapper _mapper;
        readonly string NO_PROFILE_PICS = "No profile pics in database";
        public ProfilePicsController(FitnessDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of all profile pics in the database
        /// </summary>
        /// <returns>A list of all of the profile pics in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProfilePicReadDTO>>> GetAllProfilePics()
        {
            if (_context.ProfilePics == null)
            {
                return NotFound();
            }

            List<ProfilePic> profilePics = await _context.ProfilePics.ToListAsync();
            List<ProfilePicReadDTO> profilePicReadDTOs = _mapper.Map<List<ProfilePicReadDTO>>(profilePics);

            return Ok(profilePicReadDTOs);
        }

        /// <summary>
        /// Gets a profile pic based on the given id
        /// </summary>
        /// <param name="id">The id of the profile pic</param>
        /// <returns>The profile pic with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ProfilePicReadDTO>> GetProfilePicById(int id)
        {
            if (_context.ProfilePics == null)
            {
                return NotFound(NO_PROFILE_PICS);
            }

            var profilePic = await _context.ProfilePics.FindAsync(id);

            if (profilePic == null)
            {
                return NotFound($"{NO_PROFILE_PICS} in database with id {id}");
            }

            var profilePicDTO = _mapper.Map<ProfilePicReadDTO>(profilePic);

            return Ok(profilePicDTO);
        }

        /// <summary>
        /// Updates a profile pic with given ID to the given values
        /// </summary>
        /// <param name="id">The id of the profile pic that needs to be updated</param>
        /// <param name="profilePic">All the values required for the updated profile pic</param>
        /// <returns>An action result that represents how the profile pic update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProfilePic(int id, [FromBody] ProfilePicUpdateDTO profilePic)
        {
            if (!ProfilePicExists(id))
            {
                return NotFound($"{NO_PROFILE_PICS} with id {id}");
            }

            var profilePicById = await _context.ProfilePics.FindAsync(id);

            try
            {
                CreatePicToProfilePicDTO dto = new CreatePicToProfilePicDTO(profilePic.Name, profilePic.Type, profilePic.Pic);
                _context.Entry(profilePicById).CurrentValues.SetValues(dto);
                _context.Entry(profilePicById).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new profile pic to the database
        /// </summary>
        /// <param name="profilePic">The values for the new profile pic that needs to be added</param>
        /// <returns>An action result that represents how the profile pic adding went</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<ProfilePicReadDTO>> AddProfilePic([FromBody] ProfilePicCreateDTO profilePic)
        {
            CreatePicToProfilePicDTO dto = new CreatePicToProfilePicDTO(profilePic.Name, profilePic.Type, profilePic.Pic);
            var domainProfilePic = _mapper.Map<ProfilePic>(dto);
            try
            {
                _context.ProfilePics.Add(domainProfilePic);

                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Problem($"POST request failed because of an internal server error: {e.Message}");
            }

            var readProfilePicDTO = _mapper.Map<ProfilePicReadDTO>(domainProfilePic);

            return CreatedAtAction("GetProfilePicById", new { Id = readProfilePicDTO.Id }, readProfilePicDTO);
        }

        /// <summary>
        /// Removes a profile pic from the database with given ID
        /// </summary>
        /// <param name="id">The ID for the profile pic that needs to be deleted</param>
        /// <returns>An action result that represents how the profile pic deletion went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfilePic(int id)
        {
            var profilePic = await _context.ProfilePics.FindAsync(id);
            if (profilePic == null)
            {
                return NotFound($"{NO_PROFILE_PICS} with id {id}");
            }

            try
            {
                _context.ProfilePics.Remove(profilePic);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("DELETE request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Helper method to check whether the profile pic exists in the database
        /// </summary>
        /// <param name="id">The id of the profile pic to be checked</param>
        /// <returns>True if profile pic exists, but false if it doesn't</returns>
        [NonAction]
        private bool ProfilePicExists(int id)
        {
            return (_context.ProfilePics?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
