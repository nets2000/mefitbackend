﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.Profile
{
    public class ProfileReadDTO
    {
        public int Id { get; set; }

        public float Weight { get; set; }

        public float Height { get; set; }

        public string MedicalConditions { get; set; }

        public string Disabilities { get; set; }

        public string FitnessEvaluation { get; set; }

        public int UserId { get; set; }

        public int AddressId { get; set; }
    }
}
