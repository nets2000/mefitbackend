﻿namespace MeFit.Models.DTO.ProfileWorkouts
{
    public class ProfileWorkoutReadDTO
    {
        public bool Complete { get; set; }
    }
}
