﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.Workout
{
    public class WorkoutUpdateDTO
    {
        public int Id { get; set; }
        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string? Name { get; set; }

        [MaxLength(100, ErrorMessage = "Type contains too many characters. Max length is: 100")]
        public string? Type { get; set; }

        public ICollection<int>? ExerciseIds { get; set; }
    }
}
