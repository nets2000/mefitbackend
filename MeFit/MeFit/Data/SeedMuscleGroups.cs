using MeFit.Models.Domain;

namespace MeFit.Data;

public class SeedMuscleGroups
{
    public static List<MuscleGroup> GetSeedingMuscleGroups()
    {
        List<MuscleGroup> muscleGroupsSeed = new List<MuscleGroup>()
        {
            new MuscleGroup { Id = 1, Name = "Shoulders" },
            new MuscleGroup { Id = 2, Name = "Upper back" },
            new MuscleGroup { Id = 3, Name = "Lower back" },
            new MuscleGroup { Id = 4, Name = "Glutes" },
            new MuscleGroup { Id = 5, Name = "Quads" },
            new MuscleGroup { Id = 6, Name = "Hamstrings" },
            new MuscleGroup { Id = 7, Name = "Calves" },
            new MuscleGroup { Id = 8, Name = "Chest" },
            new MuscleGroup { Id = 9, Name = "Biceps" },
            new MuscleGroup { Id = 10, Name = "Triceps" },
            new MuscleGroup { Id = 11, Name = "Abs" },
            new MuscleGroup { Id = 12, Name = "Cardio" }
        };
        return muscleGroupsSeed;
    }
}