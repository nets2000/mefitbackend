﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class Goal
    {
        [Key]
        public int Id { get; set; }
        public DateTime EndDate { get; set; }
        [DefaultValue(false)]
        public bool Achieved { get; set; }
        [DefaultValue(true)]
        public bool Current { get; set; }

        public int? FitnessProgramId { get; set; }

        public FitnessProgram? FitnessProgram { get; set; }

        public ICollection<GoalWorkout>? GoalWorkouts { get; set; }
        public ICollection<GoalExercise>? GoalExercises { get; set; }
        public int ProfileId { get; set; }
    }
}
