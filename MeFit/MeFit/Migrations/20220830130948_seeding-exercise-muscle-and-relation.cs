﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class seedingexercisemuscleandrelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExerciseMuscleGroup",
                table: "ExerciseMuscleGroup");

            migrationBuilder.DropIndex(
                name: "IX_ExerciseMuscleGroup_MuscleGroupsId",
                table: "ExerciseMuscleGroup");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExerciseMuscleGroup",
                table: "ExerciseMuscleGroup",
                columns: new[] { "MuscleGroupsId", "ExercisesId" });

            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "Image", "Name", "VidLink" },
                values: new object[,]
                {
                    { 1, "A lower-body strength training exercise defined by lifting your lower back and torso with your knees bent and your upper body resting on a bench", "", "Barbel Hip Thrust", "https://youtu.be/un4sFDkGlVY" },
                    { 2, "", "", "Hyperextension for Glutes", "https://youtu.be/IU-PJkLoHio" },
                    { 3, "A loaded barbell is lifted off the ground to the level of the hips, torso perpendicular to the floor, before being placed back on the ground", "", "Barbell Deadlift", "https://youtu.be/234EbYt2U7c" },
                    { 4, "", "", "Lying Leg Curl", "https://youtu.be/GLbTloEDiNM" },
                    { 5, "", "", "Leg Extensions", "https://youtu.be/5UBkMcdFDjU" },
                    { 6, "", "", "Ab Wheel Rollout", "https://youtu.be/m6cYbFjP-U4" },
                    { 7, "", "", "Wide grip pull up", "https://youtu.be/2RcE90XDK3Q" },
                    { 8, "", "", "Seated cable row", "https://youtu.be/6UEvt8KXyQY" },
                    { 9, "", "", "Seated dumbbell shoulder press", "https://youtu.be/QFxBA43GUFM" },
                    { 10, "", "", "Seated dumbbell side raise", "https://youtu.be/u88uvszRoZI" },
                    { 11, "", "", "Bent over dumbbell lateral raise", "https://youtu.be/dN1k-MWLHT4" },
                    { 12, "", "", "Incline dumbbell curl", "https://youtu.be/XChhbTXx7hY" },
                    { 13, "", "", "Dumbbell kickback", "https://youtu.be/PBFqCPI85nw" },
                    { 14, "", "", "Back squat", "https://youtu.be/b1OVoeazIJY" },
                    { 15, "", "", "Bulgarian split squat", "https://youtu.be/W8X6SroCQC0" },
                    { 16, "", "", "Machine hip abduction", "https://youtu.be/tl7rVxWrEFw" },
                    { 17, "", "", "Incline sit ups", "https://youtu.be/gtXGLp3llTw" },
                    { 18, "", "", "Romanian deadlift", "https://youtu.be/m8phGf0SN_Q" },
                    { 19, "", "", "Walking dumbbell lunges", "https://youtu.be/5fqt7SZBz8I" },
                    { 20, "", "", "Standing calf raise", "https://youtu.be/lSmvqCevJe8" },
                    { 21, "", "", "Reverse crunch", "https://www.youtube.com/watch?v=ujTWhn2Mvsc" },
                    { 22, "", "", "Front lat pulldown", "https://youtu.be/MgP3V0LCLjM" },
                    { 23, "", "", "Seated dumbbell shoulder press", "https://youtu.be/QFxBA43GUFM" },
                    { 24, "", "", "One arm dumbbell row", "https://youtu.be/DT_Ui3qPllY" },
                    { 25, "", "", "Machine preacher curl", "https://youtu.be/xwNr7icqFW8" },
                    { 26, "", "", "Pronated cable fly", "https://youtu.be/EzleutBmCAQ" },
                    { 27, "", "", "Cable triceps pushdown ", "https://youtu.be/bYlgQgAiWBA" },
                    { 28, "", "", "Weighted sit ups", "https://youtu.be/vlMbsyQGSD4" },
                    { 29, "", "", "Leg raises", "" },
                    { 30, "", "", "Dip assisted", "https://www.youtube.com/watch?v=8zFcJWD9Y_E" },
                    { 31, "", "", "Standing military press ", "https://youtu.be/sMZR0LoFutc" },
                    { 32, "", "", "Hyper extensions", "https://youtu.be/XHPP3uySPPA" },
                    { 33, "", "", "Tuck up", "https://youtu.be/qjVXzb3bRjs" },
                    { 34, "", "", "Assisted pull up", "https://youtu.be/4DgVajQapJs" },
                    { 35, "", "", "Seated dumbell curl", "https://youtu.be/CY2Uz5ZQyiM" },
                    { 36, "", "", "Overhead rope triceps extension", "https://youtu.be/UKbLvRZMyeM" },
                    { 37, "", "", "Incline dumbbell chest press", "https://youtu.be/BV1WgsasJU4" },
                    { 38, "", "", "Reverse dumbbell fly", "https://youtu.be/TW3z7DxBeq4" },
                    { 39, "", "", "Hanging knee raises", "https://youtu.be/hT3wq3xWZsc" },
                    { 40, "", "", "45 graden leg press (feet low)", "https://youtu.be/WDMwQiPk6Gw" },
                    { 41, "", "", "Romanian deadlift", "https://youtu.be/m8phGf0SN_Q" },
                    { 42, "", "", "Cable kickback", "https://youtu.be/FfvGyH5ugA0" }
                });

            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "Image", "Name", "VidLink" },
                values: new object[,]
                {
                    { 43, "", "", "Negative pull ups", "https://youtu.be/gbPURTSxQLY" },
                    { 44, "", "", "Standing dumbbell shoulder press", "" },
                    { 45, "", "", "Standing dumbbell side raise", "" },
                    { 46, "", "", "Biceps rope hammer curl", "https://youtu.be/hxqZ5FLqA1A" },
                    { 47, "", "", "One arm cable triceps kickback", "https://youtu.be/Brrg3OMeePA" },
                    { 48, "", "", "Machine rear delt fly", "https://youtu.be/aW5pIf2LRbw" },
                    { 49, "", "", "Swiss ball V ups", "https://youtu.be/Jbk7DfnaKXs" },
                    { 50, "", "", "Reverse barbell lunge", "https://youtu.be/R-g5yPNYv2k" },
                    { 51, "", "", "Incline machine chest press", "https://youtu.be/hdAkAv3iE00" },
                    { 52, "", "", "Dumbbell shoulder press", "https://youtu.be/m4Erm0S3n-w" },
                    { 53, "", "", "Cross cable triceps extensions", "https://youtu.be/OEYwFdaqwRM" },
                    { 54, "", "", "Power squat", "https://youtu.be/r7nEuWVgl6s" },
                    { 55, "", "", "Flat dumbbell chest press", "https://youtu.be/h_q2PiVzdSM" },
                    { 56, "", "", "Seated leg curls", "https://www.youtube.com/watch?v=A4hJynJoWUo" },
                    { 57, "", "", "Kickstand romanian deadlift", "https://youtu.be/RrLgwECd1C0" },
                    { 58, "", "", "Single leg glute bridge", "https://youtu.be/zbOBWOcvtRU" },
                    { 59, "", "", "Wide stance barbell squat", "https://youtu.be/oddwOgfxZOc" },
                    { 60, "", "", "Rowing machine circular", "https://youtu.be/d5D14-3pdnw" },
                    { 61, "", "", "Hyper extension for lower back ", "https://youtu.be/XkHNUBjCwuw" },
                    { 62, "", "", "Dumbbell spider curl", "https://youtu.be/MJuyjjfnoBo" }
                });

            migrationBuilder.InsertData(
                table: "MuscleGroups",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Shoulders" },
                    { 2, "Upper back" },
                    { 3, "Lower back" },
                    { 4, "Glutes" },
                    { 5, "Quads" },
                    { 6, "Hamstrings" },
                    { 7, "Calves" },
                    { 8, "Chest" },
                    { 9, "Biceps" },
                    { 10, "Triceps" },
                    { 11, "Abs" }
                });

            migrationBuilder.InsertData(
                table: "ExerciseMuscleGroup",
                columns: new[] { "ExercisesId", "MuscleGroupsId" },
                values: new object[,]
                {
                    { 7, 1 },
                    { 9, 1 },
                    { 10, 1 },
                    { 22, 1 },
                    { 23, 1 },
                    { 31, 1 },
                    { 34, 1 },
                    { 43, 1 },
                    { 44, 1 },
                    { 45, 1 },
                    { 52, 1 },
                    { 7, 2 },
                    { 8, 2 },
                    { 11, 2 },
                    { 22, 2 },
                    { 24, 2 },
                    { 34, 2 },
                    { 38, 2 },
                    { 43, 2 },
                    { 48, 2 },
                    { 60, 2 },
                    { 2, 3 },
                    { 3, 3 },
                    { 18, 3 },
                    { 32, 3 },
                    { 41, 3 },
                    { 57, 3 },
                    { 61, 3 },
                    { 1, 4 },
                    { 2, 4 },
                    { 3, 4 },
                    { 14, 4 },
                    { 15, 4 },
                    { 16, 4 },
                    { 18, 4 },
                    { 19, 4 },
                    { 32, 4 },
                    { 41, 4 },
                    { 42, 4 },
                    { 50, 4 },
                    { 54, 4 },
                    { 57, 4 }
                });

            migrationBuilder.InsertData(
                table: "ExerciseMuscleGroup",
                columns: new[] { "ExercisesId", "MuscleGroupsId" },
                values: new object[,]
                {
                    { 58, 4 },
                    { 59, 4 },
                    { 61, 4 },
                    { 1, 5 },
                    { 5, 5 },
                    { 14, 5 },
                    { 15, 5 },
                    { 19, 5 },
                    { 40, 5 },
                    { 50, 5 },
                    { 54, 5 },
                    { 59, 5 },
                    { 4, 6 },
                    { 56, 6 },
                    { 58, 6 },
                    { 20, 7 },
                    { 26, 8 },
                    { 37, 8 },
                    { 51, 8 },
                    { 55, 8 },
                    { 12, 9 },
                    { 25, 9 },
                    { 35, 9 },
                    { 46, 9 },
                    { 62, 9 },
                    { 13, 10 },
                    { 27, 10 },
                    { 30, 10 },
                    { 36, 10 },
                    { 47, 10 },
                    { 53, 10 },
                    { 6, 11 },
                    { 17, 11 },
                    { 21, 11 },
                    { 28, 11 },
                    { 29, 11 },
                    { 33, 11 },
                    { 39, 11 },
                    { 49, 11 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseMuscleGroup_ExercisesId",
                table: "ExerciseMuscleGroup",
                column: "ExercisesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExerciseMuscleGroup",
                table: "ExerciseMuscleGroup");

            migrationBuilder.DropIndex(
                name: "IX_ExerciseMuscleGroup_ExercisesId",
                table: "ExerciseMuscleGroup");

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 7, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 9, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 10, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 22, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 23, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 31, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 34, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 43, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 44, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 45, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 52, 1 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 7, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 8, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 11, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 22, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 24, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 34, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 38, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 43, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 48, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 60, 2 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 3, 3 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 18, 3 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 32, 3 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 41, 3 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 57, 3 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 61, 3 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 14, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 15, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 16, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 18, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 19, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 32, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 41, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 42, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 50, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 54, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 57, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 58, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 59, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 61, 4 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 5, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 14, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 15, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 19, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 40, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 50, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 54, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 59, 5 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 4, 6 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 56, 6 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 58, 6 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 20, 7 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 26, 8 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 37, 8 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 51, 8 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 55, 8 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 12, 9 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 25, 9 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 35, 9 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 46, 9 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 62, 9 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 13, 10 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 27, 10 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 30, 10 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 36, 10 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 47, 10 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 53, 10 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 6, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 17, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 21, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 28, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 29, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 33, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 39, 11 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 49, 11 });

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExerciseMuscleGroup",
                table: "ExerciseMuscleGroup",
                columns: new[] { "ExercisesId", "MuscleGroupsId" });

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseMuscleGroup_MuscleGroupsId",
                table: "ExerciseMuscleGroup",
                column: "MuscleGroupsId");
        }
    }
}
