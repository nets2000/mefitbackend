﻿using System.ComponentModel;

namespace MeFit.Models.Domain
{
    public class GoalWorkout
    {
        public int GoalId { get; set; }
        public Goal Goal { get; set; }
        public int WorkoutId { get; set; }
        public Workout Workout { get; set; }
        [DefaultValue(false)]
        public bool Complete { get; set; }
    }
}
