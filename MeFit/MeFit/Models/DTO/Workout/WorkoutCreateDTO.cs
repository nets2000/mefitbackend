﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.DTO.Workout
{
    public class WorkoutCreateDTO
    {

        [MaxLength(100, ErrorMessage = "Name contains too many characters. Max length is: 100")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Type contains too many characters. Max length is: 100")]
        public string Type { get; set; }

        public int UserId { get; set; }

        public ICollection<int>? FitnessProgramIds { get; set; }

        public ICollection<int>? ExerciseIds { get; set; }
    }
}
