﻿using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.Exercise;
using MeFit.Models.DTO.Goal;
using MeFit.Models.DTO.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class WorkoutsController : ControllerBase
    {
        private readonly string NO_WORKOUTS = "Database contains no workouts";
        private readonly FitnessDbContext _context;
        private readonly IMapper _mapper;

        public WorkoutsController(FitnessDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all workouts from the database
        /// </summary>
        /// <returns>A list of all workouts in the database</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WorkoutReadDTO>>> GetAllWorkouts()
        {
            var workouts = await _context.Workouts.ToListAsync();
            if (workouts == null)
            {
                return NotFound(NO_WORKOUTS);
            }
            return Ok(_mapper.Map<List<Workout>, IEnumerable<WorkoutReadDTO>>(workouts));
        }

        /// <summary>
        /// Get a workout with a specific id from the database
        /// </summary>
        /// <param name="id">WorkoutId to retrieve from the database</param>
        /// <returns>A workout with the given id if found</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkoutReadDTO>> GetWorkoutById(int id)
        {
            if (_context.Workouts == null)
            {
                return NotFound(NO_WORKOUTS);
            }
            var workout = _mapper.Map<WorkoutReadDTO>(await _context.Workouts.FindAsync(id));

            if (workout == null)
            {
                return NotFound($"{NO_WORKOUTS} with id {id}");
            }

            return Ok(workout);
        }

        /// <summary>
        /// Gets all the exercises in a workout
        /// </summary>
        /// <param name="id">The id of the workout that needs to show all the exercises</param>
        /// <returns>A list of exercises that are in the workout</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/exercises")]
        public async Task<ActionResult<IEnumerable<ExerciseReadDTO>>> GetAllExercisesFromWorkoutById(int id)
        {
            Workout workout = await _context.Workouts.Include(w => w.Exercises).Where(w => w.Id == id).FirstOrDefaultAsync();
            if (workout == null)
            {
                return NotFound($"{NO_WORKOUTS} with id {id}");
            }

            WorkoutAllExercisesReadDTO dto = _mapper.Map<WorkoutAllExercisesReadDTO>(workout);

            var exercises = await GetExerciseListFromIds(dto.ExerciseIds.ToList());
            List<ExerciseReadDTO> exerciseDTOs = _mapper.Map<List<ExerciseReadDTO>>(exercises);

            return Ok(exerciseDTOs);
        }

        /// <summary>
        /// Update a workout in the database. ONLY USERS WITH CONTRIBUTOR ROLE MAY UPDATE
        /// </summary>
        /// <param name="id">WorkoutId to update in database</param>
        /// <param name="workout">Updated values to replace with current ones in database</param>
        /// <returns>ActionResult with statuscode 400 if id doesnt match between query and body, 404 if database contains no
        /// workout with given id or 204 if PATCH was succesful</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateWorkout(int id, WorkoutUpdateDTO workout)
        {
            if (id != workout.Id)
            {
                return BadRequest($"Request id {id} doesn't match body id {workout.Id}");
            }
            try
            {
                Workout oldWorkout = await _context.Workouts.FindAsync(id);
                if (oldWorkout != null)
                {
                    _context.Entry(oldWorkout).CurrentValues.SetValues(UpdateValues(oldWorkout, workout));
                 }
                await _context.SaveChangesAsync();
            }
            catch
            {
                if (!WorkoutExists(id))
                {
                    return NotFound(NO_WORKOUTS + $" with id {id}");
                }
                else
                {
                    throw;
                    return Problem("PATCH request failed because of an internal server error");
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Update the exercises of the workout. ONLY USERS WITH CONTRIBUTOR ROLE MAY UPDATE
        /// </summary>
        /// <param name="id">The id of the workout that needs to be updated</param>
        /// <param name="exerciseIds">The ids of exercises that the workout needs to have</param>
        /// <returns>An action result of how the update went</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = "Administrator, Contributor")]
        [HttpPut("{id}/exercises")]
        public async Task<ActionResult> UpdateExercisesFromWorkout(int id, List<int> exerciseIds)
        {
            Workout selectedWorkout = await _context.Workouts
                .Include(w => w.Exercises)
                    .Where(w => w.Id == id).FirstOrDefaultAsync();

            if (selectedWorkout == null)
            {
                return NotFound($"{NO_WORKOUTS} with id {id}");
            }

            try
            {
                List<Exercise> selectedExercises = await GetExerciseListFromIds(exerciseIds);

                selectedWorkout.Exercises = selectedExercises;
                _context.Entry(selectedWorkout).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("PUT request failed because of an internal server error");
            }

            return NoContent();
        }

        /// <summary>
        /// Completes a workout for a goal
        /// </summary>
        /// <param name="id">The workout id to complete</param>
        /// <param name="goal">The goal values that requested to change the complete</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}/complete")]
        public async Task<ActionResult> CompleteWorkoutFromGoal(int id, GoalCompleteWorkoutDTO goal)
        {
            if (!WorkoutExists(id))
            {
                return NotFound($"{NO_WORKOUTS} with id {id}");
            }
            Goal selectedGoal = await _context.Goals.FindAsync(goal.GoalId);
            if (selectedGoal == null)
            {
                return NotFound($"No goals in database with id {goal.GoalId}");
            }
            var profileWorkout = _context.GoalWorkout.Where(profileWorkout => profileWorkout.WorkoutId == id && profileWorkout.GoalId == selectedGoal.Id).FirstOrDefault();
            if (profileWorkout == null)
            {
                return NotFound($"No workouts with workout id {id} matching a profile found");
            }
            profileWorkout.Complete = goal.Complete;
            _context.Entry(profileWorkout).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Problem("PUT request failed because of an internal server error");
            }
            return NoContent();
        }

        /// <summary>
        /// Create a new workout in the database. ONLY USERS WITH CONTRIBUTOR ROLE MAY CREATE
        /// </summary>
        /// <param name="workout">Workout to add to the database</param>
        /// <returns>The added workout object if POST was succesful, or a Problem if not</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = "Contributor")]
        [HttpPost]
        public async Task<ActionResult<WorkoutReadDTO>> AddWorkout(WorkoutCreateDTO workout)
        {
            if (_context.Workouts == null)
            {
                return NotFound(NO_WORKOUTS);
            }
            try
            {
                Workout newWorkout = _mapper.Map<Workout>(workout);
                newWorkout.User = await _context.Users.FindAsync(workout.UserId);
                newWorkout.Exercises = new List<Exercise>();
                if (workout.ExerciseIds != null)
                {
                    foreach (int id in workout.ExerciseIds)
                    {
                        var exercise = await _context.Exercises.FindAsync(id);
                        if (exercise != null)
                        {
                            newWorkout.Exercises.Add(exercise);
                        }
                    }
                }
                _context.Workouts.Add(newWorkout);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetWorkoutById", new { id = newWorkout.Id }, workout);
            }
            catch
            {
                return Problem("POST request failed because of an internal server error");
            }
        }

        /// <summary>
        /// Deletes a workout with a given id from the database. ONLY USERS WITH CONTRIBUTOR ROLE MAY DELETE
        /// </summary>
        /// <param name="id">WorkoutId to delete from database</param>
        /// <returns>ActionResult with statuscode 404 if database contains no workouts or workout with given id, or 200 if DELETE
        /// was succesful</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Administrator, Contributor")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkout(int id)
        {
            var workout = await _context.Workouts.FindAsync(id);
            if (workout == null)
            {
                return NotFound($"{NO_WORKOUTS} with id {id}");
            }
            try
            {
                _context.Workouts.Remove(workout);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return Problem("DELETE request failed because of an internal server error");
            }
            return NoContent();
        }

        /// <summary>
        /// Check whether a workout exists
        /// </summary>
        /// <param name="id">The id that need to be checked</param>
        /// <returns>True if the workout exists else it returns false</returns>
        [NonAction]
        private bool WorkoutExists(int id)
        {
            return (_context.Workouts?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Get all the exercises with the given ids
        /// </summary>
        /// <param name="exerciseIds">A list of exercise ids</param>
        /// <returns>A list of exercises based on the given ids</returns>
        [NonAction]
        private async Task<List<Exercise>> GetExerciseListFromIds(List<int> exerciseIds)
        {
            List<Exercise> selectedExercises = new List<Exercise>();

            for (int i = 0; i < exerciseIds.Count; i++)
            {
                selectedExercises.Add(await _context.Exercises.FindAsync(exerciseIds[i]));
            }

            return selectedExercises;
        }

        /// <summary>
        /// Checks for updated values and applies only those to the returned object
        /// </summary>
        /// <param name="originalWorkout">The original workout</param>
        /// <param name="updatedWorkout">The updated values</param>
        /// <returns>A new workout with only those values changed which were provided by updated workout</returns>
        [NonAction]
        private object UpdateValues(object originalWorkout, object updatedWorkout)
        {
            foreach (var property in updatedWorkout.GetType().GetProperties())
            {
                if (property.GetValue(updatedWorkout) == null)
                {
                    property.SetValue(updatedWorkout, originalWorkout.GetType().GetProperty(property.Name).GetValue(originalWorkout));
                }
            }
            return updatedWorkout;

        }
    }
}
