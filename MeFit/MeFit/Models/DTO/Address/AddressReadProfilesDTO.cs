﻿namespace MeFit.Models.DTO.Address
{
    public class AddressReadProfilesDTO
    {
        public int Id { get; set; }

        public ICollection<int> ProfileIds { get; set; }
    }
}
