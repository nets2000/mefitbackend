﻿namespace MeFit.Models.DTO.Goal
{
    public class GoalCreateDTO
    {
        public DateTime EndDate { get; set; }

        public int? FitnessProgramId { get; set; }

        public List<int>? WorkoutIds { get; set; }
        public List<int>? ExerciseIds { get; set; }
        public int ProfileId { get; set; }
    }
}
