﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class ProfilePic
    {
        public int Id { get; set; }
        [MaxLength(500, ErrorMessage = "Name contains too many characters. Max length is: 500")]
        public string Name { get; set; }
        [MaxLength(500, ErrorMessage = "Type contains too many characters. Max length is: 500")]
        public string Type { get; set; }

        public byte[]? Pic { get; set; }
    }
}
