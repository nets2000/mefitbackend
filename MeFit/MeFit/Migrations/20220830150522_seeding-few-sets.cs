﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class seedingfewsets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExerciseSet");

            migrationBuilder.DropColumn(
                name: "ExerciseRepetitions",
                table: "Sets");

            migrationBuilder.AddColumn<int>(
                name: "ExerciseId",
                table: "Sets",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxReps",
                table: "Sets",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinReps",
                table: "Sets",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Reps",
                table: "Sets",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 61,
                column: "Name",
                value: "Hyper extension for lower back");

            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "Image", "Name", "VidLink" },
                values: new object[,]
                {
                    { 63, "2 km rowing in 10 minutes", "", "Rowing warmup", "" },
                    { 64, "20 minutes on crosstrainer", "", "Crosstrainer", "" },
                    { 65, "4,5 km cycling in 10 minutes", "", "Cycling warmup", "" },
                    { 66, "Jog 3 x 400 meter", "", "Jogging", "" },
                    { 67, "Choose a cardio machine and burn 100 kcal in 10 minutes", "", "Cardio warmup", "" },
                    { 68, "1 km rowing in 5 minutes", "", "Rowing warmup", "" },
                    { 69, "Choose a cardio machine and burn 50 kcal in 5 minutes", "", "Cardio warmup", "" },
                    { 70, "Choose a cardio machine for 3 minutes", "", "Cardio warmup", "" },
                    { 71, "1200 meter rowing", "", "Rowing warmup", "" },
                    { 72, "30 minutes on crosstrainer", "", "Crosstrainer", "" },
                    { 73, "15 minutes on stairclimber", "", "Stairclimber", "" },
                    { 74, "Jog for 6 minutes", "", "Jogging", "" }
                });

            migrationBuilder.InsertData(
                table: "MuscleGroups",
                columns: new[] { "Id", "Name" },
                values: new object[] { 12, "Cardio" });

            migrationBuilder.InsertData(
                table: "Sets",
                columns: new[] { "Id", "ExerciseId", "MaxReps", "MinReps", "Reps", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 1, 8, 6, null, null },
                    { 2, 2, 10, 8, null, null },
                    { 3, 3, 12, 10, null, null },
                    { 4, 4, 15, 12, null, null },
                    { 5, 5, 20, 15, null, null },
                    { 6, 6, 12, 10, null, null }
                });

            migrationBuilder.InsertData(
                table: "ExerciseMuscleGroup",
                columns: new[] { "ExercisesId", "MuscleGroupsId" },
                values: new object[,]
                {
                    { 63, 12 },
                    { 64, 12 },
                    { 65, 12 },
                    { 66, 12 },
                    { 67, 12 },
                    { 68, 12 },
                    { 69, 12 },
                    { 70, 12 },
                    { 71, 12 },
                    { 72, 12 },
                    { 73, 12 },
                    { 74, 12 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sets_ExerciseId",
                table: "Sets",
                column: "ExerciseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sets_Exercises_ExerciseId",
                table: "Sets",
                column: "ExerciseId",
                principalTable: "Exercises",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sets_Exercises_ExerciseId",
                table: "Sets");

            migrationBuilder.DropIndex(
                name: "IX_Sets_ExerciseId",
                table: "Sets");

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 63, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 64, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 65, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 66, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 67, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 68, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 69, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 70, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 71, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 72, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 73, 12 });

            migrationBuilder.DeleteData(
                table: "ExerciseMuscleGroup",
                keyColumns: new[] { "ExercisesId", "MuscleGroupsId" },
                keyValues: new object[] { 74, 12 });

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Sets",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "MuscleGroups",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DropColumn(
                name: "ExerciseId",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "MaxReps",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "MinReps",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "Reps",
                table: "Sets");

            migrationBuilder.AddColumn<int>(
                name: "ExerciseRepetitions",
                table: "Sets",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ExerciseSet",
                columns: table => new
                {
                    ExercisesId = table.Column<int>(type: "int", nullable: false),
                    SetsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseSet", x => new { x.ExercisesId, x.SetsId });
                    table.ForeignKey(
                        name: "FK_ExerciseSet_Exercises_ExercisesId",
                        column: x => x.ExercisesId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExerciseSet_Sets_SetsId",
                        column: x => x.SetsId,
                        principalTable: "Sets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 61,
                column: "Name",
                value: "Hyper extension for lower back ");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseSet_SetsId",
                table: "ExerciseSet",
                column: "SetsId");
        }
    }
}
