﻿namespace MeFit.Models.DTO.Goal
{
    public class GoalCompleteExerciseDTO
    {
        public int GoalId { get; set; }
        public bool Complete { get; set; }
    }
}
