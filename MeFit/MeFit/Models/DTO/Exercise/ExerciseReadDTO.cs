﻿namespace MeFit.Models.DTO.Exercise
{
    public class ExerciseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public int? TotalSets { get; set; }

        public int? Reps { get; set; }

        public int? MinReps { get; set; }

        public int? MaxReps { get; set; }

        public string Image { get; set; }

        public string VidLink { get; set; }

        public ICollection<int> WorkoutIds { get; set; }

        public ICollection<int> MuscleGroupIds { get; set; }
    }
}
