﻿namespace MeFit.Models.DTO.Goal
{
    public class GoalUpdateDTO
    {
        public int Id { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? Achieved { get; set; }

        public bool? Current { get; set; }

        public int? FitnessProgramId { get; set; }
        public int ProfileId { get; set; }
        public List<int>? Workouts { get; set; }
    }
}
