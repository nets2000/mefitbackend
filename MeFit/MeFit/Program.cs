using AutoMapper;
using MeFit.Data;
using MeFit.Profiles;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
{
    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
    hostingContext.Configuration.GetConnectionString("DefaultConnection");
});
// Add automapper
var mapperConfig = new MapperConfiguration(mc =>
{
    mc.AddProfile(new ProfileProfile());
    mc.AddProfile(new ExerciseProfile());
    mc.AddProfile(new FitnessProgramProfile());
    mc.AddProfile(new UserProfile());
    mc.AddProfile(new GoalsProfile());
    mc.AddProfile(new WorkoutsProfile());
    mc.AddProfile(new AddressProfile());
    mc.AddProfile(new MuscleGroupProfile());
    mc.AddProfile(new ProfilePicProfile());
});
IMapper mapper = mapperConfig.CreateMapper();
// Add services to the container.
builder.Services.AddSingleton(mapper);
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddControllers();
builder.Services.AddDbContext<FitnessDbContext>(opt =>
{
    opt.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "MeFitAPI",
        Version = "v1",
        Description = "A database for the saving of fitness users and exercises"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});
builder.Services.AddRouting(configureOptions => configureOptions.LowercaseUrls = true);
builder.Services.AddDbContext<FitnessDbContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
                      policy =>
                      {
                          policy.WithOrigins("http://localhost:4200", "https://mefitapiexperis.azurewebsites.net", "https://mefit.azurewebsites.net")
                          .AllowAnyHeader()
                          .AllowAnyMethod();
                      });
});

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
        {
            var client = new HttpClient();
            var keyuri = builder.Configuration["TokenSecrets:KeyURI"];
            //Retrieves the keys from keycloak instance to verify token
            var response = client.GetAsync(keyuri).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
            return keys.Keys;
        },

        ValidIssuers = new List<string>
       {
                    builder.Configuration["TokenSecrets:IssuerURI"]
       },

        //This checks the token for a the 'aud' claim value
        ValidAudience = "account",
    };
    
});

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Administrator", policy => policy.RequireClaim("user_roles", "[Administrator]"));
    options.AddPolicy("Contributor", policy => policy.RequireClaim("user_roles", "[Contributor]"));
    options.AddPolicy("User", policy => policy.RequireClaim("user_roles", "[User]"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
