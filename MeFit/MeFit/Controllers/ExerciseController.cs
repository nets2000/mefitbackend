using AutoMapper;
using MeFit.Data;
using MeFit.Models.Domain;
using MeFit.Models.DTO.Exercise;
using MeFit.Models.DTO.Goal;
using MeFit.Models.DTO.MuscleGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace MeFit.Controllers;

[Route("api/[controller]")]
[ApiController]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
[Authorize]
public class ExerciseController : ControllerBase
{
    private readonly FitnessDbContext _context;
    private readonly IMapper _mapper;
    readonly string NO_EXERCISES = "No exercises in database";

    public ExerciseController(FitnessDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Get all exercises
    /// </summary>
    /// <returns>A list of exercises(ExerciseReadDTO) from the database</returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<ExerciseReadDTO>>> GetAllExercises()
    {
        if (_context.Exercises == null)
        {
            return NotFound(NO_EXERCISES);
        }
        var exercises = await _context.Exercises
            .Include(e => e.Workouts)
            .Include(e => e.MuscleGroups)
            .ToListAsync();
        var readExercises = _mapper.Map<List<ExerciseReadDTO>>(exercises);

        return Ok(readExercises);
    }

    /// <summary>
    /// Get a specific exercise by id
    /// </summary>
    /// <param name="id">The id of the requested exercise</param>
    /// <returns>One exercise(ExerciseReadDTO) from the database</returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet("{id}")]
    public async Task<ActionResult<ExerciseReadDTO>> GetExerciseById(int id)
    {
        if (_context.Exercises == null)
        {
            return NotFound(NO_EXERCISES);
        }
        if (!ExerciseExists(id))
            return NotFound($"{NO_EXERCISES} + with id {id}");
        var exercise = await _context.Exercises
            .Include(e => e.Workouts)
            .Include(e => e.MuscleGroups)
            .Where(e => e.Id == id)
            .FirstOrDefaultAsync();


        var readExercise = _mapper.Map<ExerciseReadDTO>(exercise);

        return Ok(readExercise);
    }

    /// <summary>
    /// Gets all the muscle groups in a exercise
    /// </summary>
    /// <param name="id">The id of the exercise that needs to show all the muscle groups</param>
    /// <returns>A list of muscle groups that are in the exercise</returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [HttpGet("{id}/muscle-groups")]
    public async Task<ActionResult<IEnumerable<MuscleGroupReadDTO>>> GetAllMuscleGroupsFromExerciseById(int id)
    {
        if (_context.Exercises == null || _context.MuscleGroups == null)
        {
            return NotFound(NO_EXERCISES);
        }
        if (!ExerciseExists(id))
        {
            return NotFound($"{NO_EXERCISES} with id {id}");
        }
        Exercise exercise = await _context.Exercises
            .Include(e => e.MuscleGroups)
                .Where(e => e.Id == id).FirstOrDefaultAsync();
        if (exercise == null)
        {
            return NotFound($"{NO_EXERCISES} with id {id}");
        }

        ExerciseReadDTO dto = _mapper.Map<ExerciseReadDTO>(exercise);

        var muscleGroups = await GetMuscleGroupListFromIds(dto.MuscleGroupIds.ToList());
        List<MuscleGroupReadDTO> muscleGroupsDTOs = _mapper.Map<List<MuscleGroupReadDTO>>(muscleGroups);

        return Ok(muscleGroupsDTOs);
    }

    /// <summary>
    /// Adds an exercise. ONLY USERS WITH CONTRIBUTOR ROLE MAY ADD
    /// </summary>
    /// <param name="exerciseDto">All the fields(ExerciseCreateDTO) that need to be added</param>
    /// <returns>The exercise(ExerciseReadDTO) that is successfully added to the database</returns>
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [HttpPost]
    [Authorize(Roles = "Administrator, Contributor")]
    public async Task<ActionResult<ExerciseReadDTO>> AddExercise([FromBody] ExerciseCreateDTO exerciseDto)
    {
        var domainExercise = _mapper.Map<Exercise>(exerciseDto);
        var user = await _context.Users.FindAsync(exerciseDto.UserId);
        if (user == null)
        {
            return BadRequest("Provided User ID not found in database");
        }
        try
        {
            domainExercise.User = user;
            domainExercise.MuscleGroups = new List<MuscleGroup>();
            foreach (int id in exerciseDto.MusclegroupIds)
            {
                var muscleGroup = await _context.MuscleGroups.FindAsync(id);
                if (muscleGroup != null)
                {
                    domainExercise.MuscleGroups.Add(muscleGroup);
                }
            }
            _context.Exercises.Add(domainExercise);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("POST request failed because of an internal server error");
        }

        var newExercise = _mapper.Map<ExerciseReadDTO>(domainExercise);

        return CreatedAtAction("GetExerciseById", new { Id = newExercise.Id }, newExercise);
    }

    /// <summary>
    /// Updates an exercise found by id. ONLY USERS WITH CONTRIBUTOR ROLE MAY UPDATE
    /// </summary>
    /// <param name="id">The id of the exercise that needs to be updated</param>
    /// <param name="updatedExercise">All the fields(ExerciseUpdateDTO) that need to be updated</param>
    /// <returns>The updated exercise when successfully updated</returns>
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [HttpPut("{id}")]
    //[Authorize(Roles = "Administrator, Contributor")]
    public async Task<ActionResult> UpdateExercise(int id, [FromBody] ExerciseUpdateDTO updatedExercise)
    {
        if (id != updatedExercise.Id)
            return BadRequest($"Request id {id} doesn't match body id {updatedExercise.Id}");

        if (!ExerciseExists(id))
            return NotFound($"{NO_EXERCISES} with id {id}");

        try
        {
            var currentExercise = await _context.Exercises.FindAsync(updatedExercise.Id);
            var domainExercise = _mapper.Map < Exercise > (UpdateValues(currentExercise, updatedExercise));
            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("PUT request failed because of an internal server error");
        }

        return Ok(updatedExercise);
    }

    /// <summary>
    /// Update the muscle groups of the exercise. ONLY USERS WITH CONTRIBUTOR ROLE MAY UPDATE
    /// </summary>
    /// <param name="id">The id of the exercise that needs to be updated</param>
    /// <param name="muscleGroupIds">The ids of muscle groups that the exercise needs to have</param>
    /// <returns>An action result of how the update went</returns>
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [HttpPut("{id}/muscle-groups")]
    [Authorize(Roles = "Administrator, Contributor")]
    public async Task<ActionResult> UpdateMuscleGroupsFromExercise(int id, List<int> muscleGroupIds)
    {
        Exercise selectedExercise = await _context.Exercises
            .Include(e => e.MuscleGroups)
                .Where(e => e.Id == id).FirstOrDefaultAsync();

        if (selectedExercise == null)
        {
            return NotFound($"{NO_EXERCISES} with id {id}");
        }

        try
        {
            List<MuscleGroup> selectedMuscleGroup = await GetMuscleGroupListFromIds(muscleGroupIds);

            selectedExercise.MuscleGroups = selectedMuscleGroup;
            _context.Entry(selectedExercise).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("PUT request failed because of an internal server error");
        }
        return NoContent();
    }

    /// <summary>
    /// Completes a workout for a goal
    /// </summary>
    /// <param name="id">The workout id to complete</param>
    /// <param name="goal">The goal values that requested to change the complete</param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [HttpPut("{id}/complete")]
    public async Task<ActionResult> CompleteWorkoutFromGoal(int id, GoalCompleteExerciseDTO goal)
    {
        if (!ExerciseExists(id))
        {
            return NotFound($"{NO_EXERCISES} with id {id}");
        }
        Goal selectedGoal = await _context.Goals.FindAsync(goal.GoalId);
        if (selectedGoal == null)
        {
            return NotFound($"No goals in database with id {goal.GoalId}");
        }
        var profileWorkout = _context.GoalExercise.Where(profileWorkout => profileWorkout.ExerciseId == id && profileWorkout.GoalId == selectedGoal.Id).FirstOrDefault();
        if (profileWorkout == null)
        {
            return NotFound($"No workouts with workout id {id} matching a profile found");
        }
        profileWorkout.Complete = goal.Complete;
        _context.Entry(profileWorkout).State = EntityState.Modified;
        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            return Problem("PUT request failed because of an internal server error");
        }
        return NoContent();
    }

    /// <summary>
    /// Deletes an exercise by id. ONLY USERS WITH CONTRIBUTOR ROLE MAY DELETE
    /// </summary>
    /// <param name="id">The id of the exercise that needs to be deleted</param>
    /// <returns>Saves changes to the database, but doesn't return anything</returns>
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Authorize(Roles = "Administrator, Contributor")]
    public async Task<ActionResult> DeleteExercise(int id)
    {
        var exercise = await _context.Exercises.FindAsync(id);
        if (!ExerciseExists(id))
            return NotFound($"{NO_EXERCISES} with id {id}");

        try
        {
            var domainExercise = _mapper.Map<Exercise>(exercise);
            _context.Exercises.Remove(domainExercise);
            await _context.SaveChangesAsync();
        }
        catch
        {
            return Problem("DELETE request failed because of an internal server error");
        }

        return NoContent();
    }

    /// <summary>
    /// Helper method to check whether the exercise exists in the database
    /// </summary>
    /// <param name="id">The id of the exercise to be checked</param>
    /// <returns>True if exercise exists, but false if it doesn't</returns>
    [NonAction]
    private bool ExerciseExists(int id)
    {
        return _context.Exercises.Any(e => e.Id == id);
    }

    /// <summary>
    /// Get all the muscle groups with the given ids
    /// </summary>
    /// <param name="muscleGroupIds">A list of muscle groups ids</param>
    /// <returns>A list of muscle groups based on the given ids</returns>
    
    [NonAction]
    private async Task<List<MuscleGroup>> GetMuscleGroupListFromIds(List<int> muscleGroupIds)
    {
        List<MuscleGroup> selectedMuscleGroups = new List<MuscleGroup>();

        for (int i = 0; i < muscleGroupIds.Count; i++)
        {
            selectedMuscleGroups.Add(await _context.MuscleGroups.FindAsync(muscleGroupIds[i]));
        }

        return selectedMuscleGroups;
    }

    /// <summary>
    /// Updates an original object to the updated object
    /// </summary>
    /// <param name="original">The original object</param>
    /// <param name="updated">The updated object</param>
    /// <returns>The original object with the updated values</returns>
    [NonAction]
    private object UpdateValues(object original, object updated)
    {
        foreach (var property in updated.GetType().GetProperties())
        {
            if (property.GetValue(updated, null) == null)
            {
                property.SetValue(updated, original.GetType().GetProperty(property.Name).GetValue(original, null));
            }
        }

        return updated;
    }
}