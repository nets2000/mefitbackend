﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFit.Migrations
{
    public partial class goalworkoutrelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GoalWorkout_Goals_GoalsId",
                table: "GoalWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_GoalWorkout_Workouts_WorkoutsId",
                table: "GoalWorkout");

            migrationBuilder.DropTable(
                name: "ProfileWorkouts");

            migrationBuilder.RenameColumn(
                name: "WorkoutsId",
                table: "GoalWorkout",
                newName: "GoalId");

            migrationBuilder.RenameColumn(
                name: "GoalsId",
                table: "GoalWorkout",
                newName: "WorkoutId");

            migrationBuilder.RenameIndex(
                name: "IX_GoalWorkout_WorkoutsId",
                table: "GoalWorkout",
                newName: "IX_GoalWorkout_GoalId");

            migrationBuilder.AddColumn<bool>(
                name: "Complete",
                table: "GoalWorkout",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_GoalWorkout_Goals_GoalId",
                table: "GoalWorkout",
                column: "GoalId",
                principalTable: "Goals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GoalWorkout_Workouts_WorkoutId",
                table: "GoalWorkout",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GoalWorkout_Goals_GoalId",
                table: "GoalWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_GoalWorkout_Workouts_WorkoutId",
                table: "GoalWorkout");

            migrationBuilder.DropColumn(
                name: "Complete",
                table: "GoalWorkout");

            migrationBuilder.RenameColumn(
                name: "GoalId",
                table: "GoalWorkout",
                newName: "WorkoutsId");

            migrationBuilder.RenameColumn(
                name: "WorkoutId",
                table: "GoalWorkout",
                newName: "GoalsId");

            migrationBuilder.RenameIndex(
                name: "IX_GoalWorkout_GoalId",
                table: "GoalWorkout",
                newName: "IX_GoalWorkout_WorkoutsId");

            migrationBuilder.CreateTable(
                name: "ProfileWorkouts",
                columns: table => new
                {
                    ProfileId = table.Column<int>(type: "int", nullable: false),
                    WorkoutId = table.Column<int>(type: "int", nullable: false),
                    Complete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfileWorkouts", x => new { x.ProfileId, x.WorkoutId });
                    table.ForeignKey(
                        name: "FK_ProfileWorkouts_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfileWorkouts_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProfileWorkouts_WorkoutId",
                table: "ProfileWorkouts",
                column: "WorkoutId");

            migrationBuilder.AddForeignKey(
                name: "FK_GoalWorkout_Goals_GoalsId",
                table: "GoalWorkout",
                column: "GoalsId",
                principalTable: "Goals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GoalWorkout_Workouts_WorkoutsId",
                table: "GoalWorkout",
                column: "WorkoutsId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
