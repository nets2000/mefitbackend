﻿namespace MeFit.Models.DTO.MuscleGroup
{
    public class MuscleGroupExerciseReadDTO
    {
        public int Id { get; set; }

        public ICollection<int> ExerciseIds { get; set; }
    }
}
