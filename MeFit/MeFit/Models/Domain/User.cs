﻿using System.ComponentModel.DataAnnotations;

namespace MeFit.Models.Domain
{
    public class User
    {
        public int Id { get; set; }

        [MaxLength(1000, ErrorMessage = "Password contains too many characters. Max length is: 1000")]
        public string Password { get; set; }

        [MaxLength(100, ErrorMessage = "First name contains too many characters. Max length is: 100")]
        public string FirstName { get; set; }

        [MaxLength(100, ErrorMessage = "Last name contains too many characters. Max length is: 100")]
        public string LastName { get; set; }

        [MaxLength(300, ErrorMessage = "User ID contains too many characters. Max length is: 300")]
        public string UserId { get; set; }

        public bool WantsToBeContributor { get; set; }

        public bool IsContributor { get; set; }

        public bool IsAdmin { get; set; }

        public int? ProfilePicId { get; set; }

        public ProfilePic? ProfilePic { get; set; }
    }
}
